import tensorflow as tf
tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)
import os
os.environ["KMP_WARNINGS"] = "FALSE"

import gym
import glob
import gym_msr
import numpy as np
import pickle
import timeit
from stable_baselines import DQN, results_plotter
from stable_baselines.bench import Monitor
from stable_baselines.deepq.policies import MlpPolicy
from stable_baselines.results_plotter import load_results, ts2xy
from learning_tools import get_last_agent, LearningLog
import matplotlib.pyplot as plt

# Parameters used in callbacks
best_av_reward, best_win_rate, current_agent_int, eval_env, learning_log = -np.inf, -1, 0, None, None

# Evaluate a policy
def evaluate_policy(eval_env, agent, leading_rider='Rider A', n_eval_runs=25):
    """Evaluate policy with number of runs.
        Parameters:
        eval_env - Environment used for evaluation.
        agent - Agent to evaluate.
        leading_rider - Leading rider string for the set of runs.
        n_eval_runs - Number of runs to evaluate over.
        Returns:
        av_win_rate - Average win rate over runs.
        av_sep - Average separation over runs.
        av_reward - Average reward over runs.
    """

    n_wins, total_rewards, total_separation = 0, 0, 0

    for _ in range(n_eval_runs):
        info = {}
        state = eval_env.reset(leading_rider=leading_rider,
                                verbose=False)
        while not eval_env.complete():
            action, _ = agent.predict(state, deterministic=True)
            state, _, _, info = eval_env.step(action)

        # Store results
        total_rewards += info['total_reward']
        total_separation += (state[0] - state[3])
        if state[0] > state[3]:
            n_wins += 1

    return n_wins/n_eval_runs, total_separation/n_eval_runs, total_rewards/n_eval_runs


# Call using evaluation
# Evaluates policy against several test points.
def callback_eval(_locals, _globals):
    """
    Callback called at each step (for DQN an others) or after n steps (see ACER or PPO2)
    :param _locals: (dict)
    :param _globals: (dict)
    """
    global best_av_reward, best_win_rate, eval_env, learning_log

    # Evaluate after every nth reset
    repetitions = _locals['self'].env.info['repetitions']
    if repetitions % 25 == 0 and _locals['self'].env.env_steps == 0:
        print("\nEvaluating agent:\n")

        # Evaluate against all the preset policy test points.
        lead_win_rate, lead_av_reward, lead_av_separation = 0, 0, 0
        trail_win_rate, trail_av_reward, trail_av_separation = 0, 0, 0

        # Rider A is leading/Rider B trailing
        print("Rider A Leading")
        lead_win_rate, lead_av_separation, lead_av_reward =  \
            evaluate_policy(eval_env, _locals['self'], leading_rider='Rider A')
        print("Fraction of leading races won: {:.2f}\n".format(lead_win_rate))

        # Rider A trailing/Rider B leading
        print("Rider A Trailing")
        trail_win_rate, trail_av_separation, trail_av_reward =  \
            evaluate_policy(eval_env, _locals['self'], leading_rider='Rider B')
        print("Fraction of trailing races won: {:.2f}\n".format(trail_win_rate))

        # Store information in log file
        current_win_rate = 0.5 * (trail_win_rate + lead_win_rate)
        current_av_reward = 0.5 * (trail_av_reward + lead_av_reward)
        current_av_separation = 0.5 * (trail_av_separation + lead_av_separation)

        # Save new agent when evaluated policy improves
        if current_win_rate >= best_win_rate:
            # Don't save if there is no improvement in average reward.
            if current_win_rate == best_win_rate and current_av_reward <= best_av_reward:
                # Print some results to screen
                print("Overall win rate: {:.2f}\n".format(current_win_rate))
                return True
            else:
                current_learning_step = _locals['self'].num_timesteps
                _, next_agent = get_last_agent(agent_folder, 'msr_')
                _locals['self'].save(next_agent)
                best_win_rate = current_win_rate
                best_av_reward = current_av_reward
                learning_log.add_table_line('Overall Agent Performance',
                                            [current_learning_step,
                                            current_win_rate,
                                            current_av_reward,
                                            current_av_separation])
                learning_log.add_table_line('Leading Performance',
                                            [current_learning_step,
                                            lead_win_rate,
                                            lead_av_reward,
                                            lead_av_separation])
                learning_log.add_table_line('Trailing Performance',
                                            [current_learning_step,
                                            trail_win_rate,
                                            trail_av_reward,
                                            trail_av_separation])
                # Dump learning log every time new agent is saved.
                learning_log.dump()
        # Print some results to screen
        print("Overall win rate: {:.2f}\n".format(current_win_rate))
    return True


# Places to save data - folder needs to end with /
time_steps = 250000
agent_folder = os.path.dirname(os.path.abspath(__file__)) + '/continue_test/'
last_agent, next_agent = get_last_agent(agent_folder, 'msr_')
if not os.path.exists(agent_folder) or not os.path.exists(last_agent + ".zip"):
    raise RuntimeError("Could not find first agent.")
start_t = timeit.default_timer()

# Log file for learning
log_folder = agent_folder + '/log'
os.makedirs(log_folder, exist_ok=True)

# Learning log for evaluation callback
log_number = len(glob.glob1(log_folder,"*.txt"))
learning_log = LearningLog(log_folder + '/' + "learning_log_" + str(log_number) + ".txt")
learning_log.set_description("Learning run from scratch using " + str(time_steps) + " timesteps.")
learning_log.add_table('Overall Agent Performance', ['Step', 'Win Rate','Av. Reward', 'Av. Sep.'])
learning_log.add_table('Leading Performance', ['Step', 'Win Rate','Av. Reward', 'Av. Sep.'])
learning_log.add_table('Trailing Performance', ['Step', 'Win Rate','Av. Reward', 'Av. Sep.'])

# Create random sprint environment with a monitor
env = gym.make('msr-duel-fixed-v2')
eval_env = gym.make('msr-duel-fixed-v2')
monitor_file = log_folder + '/monitor_log_' + str(log_number)
env = Monitor(env, monitor_file, allow_early_resets=True)

# Load and evaluate initial state of agent.
print('Using agent: ', last_agent, '\n')
print('Initial evaluation...')
model = DQN.load(last_agent)
model.set_env(env)
lead_win_rate, lead_av_separation, lead_av_reward =  \
    evaluate_policy(eval_env, model, leading_rider='Rider A')
trail_win_rate, trail_av_separation, trail_av_reward =  \
    evaluate_policy(eval_env, model, leading_rider='Rider B')
best_av_reward = 0.5 * (lead_av_reward + trail_av_reward)
best_win_rate = 0.5 * (lead_win_rate + trail_win_rate)

# Store initial results
learning_log.add_table_line('Overall Agent Performance',
                                            [-1,
                                            0.5*(lead_win_rate + trail_win_rate),
                                            0.5*(lead_av_reward + trail_av_reward),
                                            0.5*(lead_av_separation + trail_av_separation)])
learning_log.add_table_line('Leading Performance',
                            [-1,
                            lead_win_rate,
                            lead_av_reward,
                            lead_av_separation])
learning_log.add_table_line('Trailing Performance',
                            [-1,
                            trail_win_rate,
                            trail_av_reward,
                            trail_av_separation])

# Train agent
print('\nTraining...')
model.learn(total_timesteps=time_steps, callback=callback_eval)

# Visualise training results
results_plotter.plot_results([log_folder], time_steps, results_plotter.X_TIMESTEPS, "Duelling Environment")
plt.savefig(agent_folder + '/learning_plot.png')

# Process time values
process_time = (timeit.default_timer() - start_t)/60
learning_log.add_line("Total Wall time (mins) = " + str(process_time))

# Report and save log data
learning_log.report()
learning_log.dump()
with open(log_folder + '/' + 'learning_log_' + str(log_number) + '.data', 'wb') as handle:
    pickle.dump(learning_log, handle)
