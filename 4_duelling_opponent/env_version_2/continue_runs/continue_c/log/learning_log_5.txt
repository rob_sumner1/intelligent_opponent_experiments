LEARNING LOG 

Time: 04/03/2020 17:45:41
Learning run from scratch using 250000 timesteps.

Overall Agent Performance:
 |Step        |Win Rate    |Av. Reward  |Av. Sep.    |
 |-1          |0.9         |12.25221730 |0.014455912 |

Leading Performance:
 |Step        |Win Rate    |Av. Reward  |Av. Sep.    |
 |-1          |0.92        |10.86360884 |0.012725434 |

Trailing Performance:
 |Step        |Win Rate    |Av. Reward  |Av. Sep.    |
 |-1          |0.88        |13.64082577 |0.016186390 |

Total Wall time (mins) = 52.24763426563344
