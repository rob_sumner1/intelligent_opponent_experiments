LEARNING LOG 

Time: 06/03/2020 01:19:13
Learning run from scratch using 500000 timesteps.

Overall Agent Performance:
 |Step        |Win Rate    |Av. Reward  |Av. Sep.    |
 |-1          |0.5         |11.65457171 |0.012689680 |

Leading Performance:
 |Step        |Win Rate    |Av. Reward  |Av. Sep.    |
 |-1          |0.0         |0.748412248 |-0.00299002 |

Trailing Performance:
 |Step        |Win Rate    |Av. Reward  |Av. Sep.    |
 |-1          |1.0         |22.56073117 |0.028369383 |

Total Wall time (mins) = 111.2855060068
