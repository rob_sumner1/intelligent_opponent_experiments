LEARNING LOG 

Time: 06/03/2020 14:36:08
Learning run from scratch using 500000 timesteps.

Overall Agent Performance:
 |Step        |Win Rate    |Av. Reward  |Av. Sep.    |
 |-1          |0.5         |10.21096256 |0.010673793 |

Leading Performance:
 |Step        |Win Rate    |Av. Reward  |Av. Sep.    |
 |-1          |0.0         |0.748366735 |-0.00322288 |

Trailing Performance:
 |Step        |Win Rate    |Av. Reward  |Av. Sep.    |
 |-1          |1.0         |19.67355838 |0.024570471 |

Total Wall time (mins) = 110.41096645831676
