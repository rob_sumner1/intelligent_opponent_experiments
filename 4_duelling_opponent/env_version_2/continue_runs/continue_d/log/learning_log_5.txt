LEARNING LOG 

Time: 05/03/2020 21:26:58
Learning run from scratch using 500000 timesteps.

Overall Agent Performance:
 |Step        |Win Rate    |Av. Reward  |Av. Sep.    |
 |-1          |0.06        |0.961389700 |-0.02014405 |
 |30646       |0.06        |1.037549685 |-0.02084264 |

Leading Performance:
 |Step        |Win Rate    |Av. Reward  |Av. Sep.    |
 |-1          |0.0         |0.729441244 |-0.02793964 |
 |30646       |0.0         |0.729236355 |-0.02807699 |

Trailing Performance:
 |Step        |Win Rate    |Av. Reward  |Av. Sep.    |
 |-1          |0.12        |1.193338156 |-0.01234847 |
 |30646       |0.12        |1.345863016 |-0.01360829 |

LEARNING LOG 

Time: 05/03/2020 21:26:58
Learning run from scratch using 500000 timesteps.

Overall Agent Performance:
 |Step        |Win Rate    |Av. Reward  |Av. Sep.    |
 |-1          |0.06        |0.961389700 |-0.02014405 |
 |30646       |0.06        |1.037549685 |-0.02084264 |

Leading Performance:
 |Step        |Win Rate    |Av. Reward  |Av. Sep.    |
 |-1          |0.0         |0.729441244 |-0.02793964 |
 |30646       |0.0         |0.729236355 |-0.02807699 |

Trailing Performance:
 |Step        |Win Rate    |Av. Reward  |Av. Sep.    |
 |-1          |0.12        |1.193338156 |-0.01234847 |
 |30646       |0.12        |1.345863016 |-0.01360829 |

Total Wall time (mins) = 106.25315646360008
