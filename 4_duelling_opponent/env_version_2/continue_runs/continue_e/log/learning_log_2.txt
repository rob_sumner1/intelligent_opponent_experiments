LEARNING LOG 

Time: 04/03/2020 23:56:14
Learning run from scratch using 500000 timesteps.

Overall Agent Performance:
 |Step        |Win Rate    |Av. Reward  |Av. Sep.    |
 |-1          |0.22        |1.326407197 |-0.01770310 |

Leading Performance:
 |Step        |Win Rate    |Av. Reward  |Av. Sep.    |
 |-1          |0.2         |1.143533713 |-0.01330000 |

Trailing Performance:
 |Step        |Win Rate    |Av. Reward  |Av. Sep.    |
 |-1          |0.24        |1.509280681 |-0.02210620 |

Total Wall time (mins) = 97.49925756198306
