LEARNING LOG 

Time: 02/03/2020 17:56:31
Learning run from scratch using 50000 timesteps.

Overall Agent Performance:
 |Step        |Win Rate    |Av. Reward  |Av. Sep.    |
 |-1          |0.9625      |10.61672326 |0.011588021 |

Leading Performance:
 |Step        |Win Rate    |Av. Reward  |Av. Sep.    |
 |-1          |0.925       |9.211966016 |0.008674094 |

Trailing Performance:
 |Step        |Win Rate    |Av. Reward  |Av. Sep.    |
 |-1          |1.0         |12.02148051 |0.014501948 |

Total Wall time (mins) = 18.353203656633436
