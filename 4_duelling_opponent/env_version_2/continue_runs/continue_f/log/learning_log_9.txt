LEARNING LOG 

Time: 06/03/2020 16:25:59
Learning run from scratch using 500000 timesteps.

Overall Agent Performance:
 |Step        |Win Rate    |Av. Reward  |Av. Sep.    |
 |-1          |0.06        |1.012166924 |-0.02000551 |
 |138929      |0.08        |1.050981341 |-0.01851471 |

Leading Performance:
 |Step        |Win Rate    |Av. Reward  |Av. Sep.    |
 |-1          |0.0         |0.733264761 |-0.02263816 |
 |138929      |0.0         |0.731974682 |-0.02446360 |

Trailing Performance:
 |Step        |Win Rate    |Av. Reward  |Av. Sep.    |
 |-1          |0.12        |1.291069086 |-0.01737286 |
 |138929      |0.16        |1.369988000 |-0.01256582 |

LEARNING LOG 

Time: 06/03/2020 16:25:59
Learning run from scratch using 500000 timesteps.

Overall Agent Performance:
 |Step        |Win Rate    |Av. Reward  |Av. Sep.    |
 |-1          |0.06        |1.012166924 |-0.02000551 |
 |138929      |0.08        |1.050981341 |-0.01851471 |

Leading Performance:
 |Step        |Win Rate    |Av. Reward  |Av. Sep.    |
 |-1          |0.0         |0.733264761 |-0.02263816 |
 |138929      |0.0         |0.731974682 |-0.02446360 |

Trailing Performance:
 |Step        |Win Rate    |Av. Reward  |Av. Sep.    |
 |-1          |0.12        |1.291069086 |-0.01737286 |
 |138929      |0.16        |1.369988000 |-0.01256582 |

Total Wall time (mins) = 109.37588089981655
