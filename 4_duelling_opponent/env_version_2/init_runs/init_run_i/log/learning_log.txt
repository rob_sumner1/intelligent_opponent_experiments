LEARNING LOG 

Time: 03/03/2020 22:48:27
Learning run from scratch using 2000000 timesteps.

Overall Agent Performance:
 |Step        |Win Rate    |Av. Reward  |Av. Sep.    |
 |15174       |0.0         |0.614430383 |-0.17899581 |

Leading Performance:
 |Step        |Win Rate    |Av. Reward  |Av. Sep.    |
 |15174       |0.0         |0.687928209 |-0.08233033 |

Trailing Performance:
 |Step        |Win Rate    |Av. Reward  |Av. Sep.    |
 |15174       |0.0         |0.540932557 |-0.27566129 |

LEARNING LOG 

Time: 03/03/2020 22:48:27
Learning run from scratch using 2000000 timesteps.

Overall Agent Performance:
 |Step        |Win Rate    |Av. Reward  |Av. Sep.    |
 |15174       |0.0         |0.614430383 |-0.17899581 |
 |31000       |0.05        |0.906675288 |-0.01406651 |

Leading Performance:
 |Step        |Win Rate    |Av. Reward  |Av. Sep.    |
 |15174       |0.0         |0.687928209 |-0.08233033 |
 |31000       |0.0         |0.735182271 |-0.02027391 |

Trailing Performance:
 |Step        |Win Rate    |Av. Reward  |Av. Sep.    |
 |15174       |0.0         |0.540932557 |-0.27566129 |
 |31000       |0.1         |1.078168305 |-0.00785911 |

LEARNING LOG 

Time: 03/03/2020 22:48:27
Learning run from scratch using 2000000 timesteps.

Overall Agent Performance:
 |Step        |Win Rate    |Av. Reward  |Av. Sep.    |
 |15174       |0.0         |0.614430383 |-0.17899581 |
 |31000       |0.05        |0.906675288 |-0.01406651 |
 |546954      |0.05        |0.929103561 |-0.02014236 |

Leading Performance:
 |Step        |Win Rate    |Av. Reward  |Av. Sep.    |
 |15174       |0.0         |0.687928209 |-0.08233033 |
 |31000       |0.0         |0.735182271 |-0.02027391 |
 |546954      |0.0         |0.730460264 |-0.02645029 |

Trailing Performance:
 |Step        |Win Rate    |Av. Reward  |Av. Sep.    |
 |15174       |0.0         |0.540932557 |-0.27566129 |
 |31000       |0.1         |1.078168305 |-0.00785911 |
 |546954      |0.1         |1.127746859 |-0.01383442 |

LEARNING LOG 

Time: 03/03/2020 22:48:27
Learning run from scratch using 2000000 timesteps.

Overall Agent Performance:
 |Step        |Win Rate    |Av. Reward  |Av. Sep.    |
 |15174       |0.0         |0.614430383 |-0.17899581 |
 |31000       |0.05        |0.906675288 |-0.01406651 |
 |546954      |0.05        |0.929103561 |-0.02014236 |
 |1496015     |0.075       |0.945659179 |-0.01844778 |

Leading Performance:
 |Step        |Win Rate    |Av. Reward  |Av. Sep.    |
 |15174       |0.0         |0.687928209 |-0.08233033 |
 |31000       |0.0         |0.735182271 |-0.02027391 |
 |546954      |0.0         |0.730460264 |-0.02645029 |
 |1496015     |0.0         |0.733503280 |-0.02244404 |

Trailing Performance:
 |Step        |Win Rate    |Av. Reward  |Av. Sep.    |
 |15174       |0.0         |0.540932557 |-0.27566129 |
 |31000       |0.1         |1.078168305 |-0.00785911 |
 |546954      |0.1         |1.127746859 |-0.01383442 |
 |1496015     |0.15        |1.157815078 |-0.01445152 |

LEARNING LOG 

Time: 03/03/2020 22:48:27
Learning run from scratch using 2000000 timesteps.

Overall Agent Performance:
 |Step        |Win Rate    |Av. Reward  |Av. Sep.    |
 |15174       |0.0         |0.614430383 |-0.17899581 |
 |31000       |0.05        |0.906675288 |-0.01406651 |
 |546954      |0.05        |0.929103561 |-0.02014236 |
 |1496015     |0.075       |0.945659179 |-0.01844778 |

Leading Performance:
 |Step        |Win Rate    |Av. Reward  |Av. Sep.    |
 |15174       |0.0         |0.687928209 |-0.08233033 |
 |31000       |0.0         |0.735182271 |-0.02027391 |
 |546954      |0.0         |0.730460264 |-0.02645029 |
 |1496015     |0.0         |0.733503280 |-0.02244404 |

Trailing Performance:
 |Step        |Win Rate    |Av. Reward  |Av. Sep.    |
 |15174       |0.0         |0.540932557 |-0.27566129 |
 |31000       |0.1         |1.078168305 |-0.00785911 |
 |546954      |0.1         |1.127746859 |-0.01383442 |
 |1496015     |0.15        |1.157815078 |-0.01445152 |

Total Wall time (mins) = 381.2260150421333
