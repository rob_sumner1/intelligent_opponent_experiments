#!/bin/bash

function check_memory {
    # Current time
    echo "Time = $(date +'%H:%M:%S')"

    # Calculate % of Ram Used
    total_ram=$( awk '/MemTotal/ {print $2}' < /proc/meminfo )
    available_ram=$( awk '/MemAvailable/ {print $2}' < /proc/meminfo )
    used_ram=$(echo "($total_ram-$available_ram)" | bc)
    ram_percent_used=$(echo "($used_ram*100/$total_ram)" | bc)
    echo "Percentage RAM used = " $ram_percent_used "%"

    # Calculate % available HDD
    echo "HDD Usage:"
    df -H | grep -vE '^Filesystem|tmpfs|udev' | awk '{ print $5 " " $1 }'
    echo ""
}

while true ; do check_memory | tee -a "memory_log.txt" ; sleep 60 ; done


