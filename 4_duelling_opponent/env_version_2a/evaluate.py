import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
import tensorflow as tf
tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)
import os
os.environ["KMP_WARNINGS"] = "FALSE"

import gym
from stable_baselines import DQN
from learning_tools import plot_action_separation

# Load trained model
eval_folder = os.path.dirname(os.path.abspath(__file__)) + "/pretrain_continue/pretrain_continue_a/evaluate_0/"
agent_path = eval_folder + 'msr_eval'

# Environment and model
env = gym.make('msr-duel-fixed-v2')
model = DQN.load(agent_path)
model.set_env(env)

n_races = 50

# Rider A leading
races_won = 0
for i in range(n_races):
    # Rider A is leading
    state = env.reset(leading_rider='Rider A', verbose=True)
    info = {}
    while not env.complete():
        action, _ = model.predict(state, deterministic=True)
        state, _, _, info = env.step(action)

    # Save every 10th plot.
    if i % 20 == 0:
        env.save_history(name='msr_policy')
        plot_action_separation(histories=['msr_policy.hist'],
                            suptitle='MSR Policy: Action-Separation',
                            titles=['Rider A Leads'],
                            single_althete=False,
                            save=True,
                            image_save_name= eval_folder + 'act_sep_rider_a_lead_' + str(i),
                            show=False)

    # Check if rider A has won.
    if env.state[0] > env.state[3]:
        races_won += 1
        env.save_history(name='msr_policy')
        plot_action_separation(histories=['msr_policy.hist'],
                            suptitle='MSR Policy: Action-Separation',
                            titles=['Rider A Leads'],
                            single_althete=False,
                            save=True,
                            image_save_name= eval_folder + 'act_sep_rider_a_lead_' + str(i),
                            show=False)
lead_win_rate = races_won / n_races


# Rider A trailing
races_won = 0
for i in range(n_races):
    # Rider A is leading
    state = env.reset(leading_rider='Rider B', verbose=True)
    info = {}
    while not env.complete():
        action, _ = model.predict(state, deterministic=True)
        state, _, _, info = env.step(action)

    # Save every 10th plot.
    if i % 20 == 0:
        env.save_history(name='msr_policy')
        plot_action_separation(histories=['msr_policy.hist'],
                            suptitle='MSR Policy: Action-Separation',
                            titles=['Rider A Trails'],
                            single_althete=False,
                            save=True,
                            image_save_name= eval_folder + 'act_sep_rider_a_trail_' + str(i),
                            show=False)

    # Check if rider A has won.
    if env.state[0] > env.state[3]:
        races_won += 1
        env.save_history(name='msr_policy')
        plot_action_separation(histories=['msr_policy.hist'],
                            suptitle='MSR Policy: Action-Separation',
                            titles=['Rider A Trails'],
                            single_althete=False,
                            save=True,
                            image_save_name= eval_folder + 'act_sep_rider_a_trail_' + str(i),
                            show=False)

trail_win_rate = races_won / n_races
overall_win_rate = 0.5 * (lead_win_rate + trail_win_rate)
print('Rider A Overall Win rate = ', overall_win_rate)
print('Rider A Leading Win rate = ', lead_win_rate)
print('Rider A Trailing Win rate = ', trail_win_rate)
