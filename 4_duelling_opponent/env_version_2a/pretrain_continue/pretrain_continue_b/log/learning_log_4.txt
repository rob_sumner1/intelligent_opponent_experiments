LEARNING LOG 

Time: 10/03/2020 09:19:41
Learning run from scratch using 1000000 timesteps.

Overall Agent Performance:
 |Step        |Win Rate    |Av. Reward  |Av. Sep.    |
 |-1          |0.47        |10.99667291 |0.000489247 |

Leading Performance:
 |Step        |Win Rate    |Av. Reward  |Av. Sep.    |
 |-1          |0.0         |0.732537721 |-0.02391546 |

Trailing Performance:
 |Step        |Win Rate    |Av. Reward  |Av. Sep.    |
 |-1          |0.94        |21.26080810 |0.024893956 |

Total Wall time (mins) = 165.3362712547
