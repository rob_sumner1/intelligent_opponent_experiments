LEARNING LOG 

Time: 10/03/2020 03:52:44
Learning run from scratch using 1000000 timesteps.

Overall Agent Performance:
 |Step        |Win Rate    |Av. Reward  |Av. Sep.    |
 |-1          |0.47        |11.37848959 |0.001031669 |

Leading Performance:
 |Step        |Win Rate    |Av. Reward  |Av. Sep.    |
 |-1          |0.0         |0.732729211 |-0.02351361 |

Trailing Performance:
 |Step        |Win Rate    |Av. Reward  |Av. Sep.    |
 |-1          |0.94        |22.02424998 |0.025576949 |

Total Wall time (mins) = 158.12795143991661
