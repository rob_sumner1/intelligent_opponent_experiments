LEARNING LOG 

Time: 10/03/2020 13:12:59
Learning run from scratch using 250000 timesteps.

Overall Agent Performance:
 |Step        |Win Rate    |Av. Reward  |Av. Sep.    |
 |-1          |0.4         |2.492168453 |-0.00514198 |

Leading Performance:
 |Step        |Win Rate    |Av. Reward  |Av. Sep.    |
 |-1          |0.0         |0.744413689 |-0.00830214 |

Trailing Performance:
 |Step        |Win Rate    |Av. Reward  |Av. Sep.    |
 |-1          |0.8         |4.239923217 |-0.00198181 |

Total Wall time (mins) = 42.44912548831667
