LEARNING LOG 

Time: 10/03/2020 15:20:46
Learning run from scratch using 250000 timesteps.

Overall Agent Performance:
 |Step        |Win Rate    |Av. Reward  |Av. Sep.    |
 |-1          |0.39        |2.516290311 |-0.00404365 |

Leading Performance:
 |Step        |Win Rate    |Av. Reward  |Av. Sep.    |
 |-1          |0.0         |0.744366186 |-0.00831855 |

Trailing Performance:
 |Step        |Win Rate    |Av. Reward  |Av. Sep.    |
 |-1          |0.78        |4.288214435 |0.000231246 |

Total Wall time (mins) = 41.32294552311666
