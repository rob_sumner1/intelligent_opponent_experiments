LEARNING LOG 

Time: 09/03/2020 12:56:23
Learning run from scratch using 500000 timesteps.

Overall Agent Performance:
 |Step        |Win Rate    |Av. Reward  |Av. Sep.    |
 |-1          |0.0         |0.747796462 |-0.00369498 |
 |44271       |0.0         |0.747961723 |-0.00354258 |

Leading Performance:
 |Step        |Win Rate    |Av. Reward  |Av. Sep.    |
 |-1          |0.0         |0.748049433 |-0.00344866 |
 |44271       |0.0         |0.747819355 |-0.00366819 |

Trailing Performance:
 |Step        |Win Rate    |Av. Reward  |Av. Sep.    |
 |-1          |0.0         |0.747543492 |-0.00394129 |
 |44271       |0.0         |0.748104091 |-0.00341697 |

LEARNING LOG 

Time: 09/03/2020 12:56:23
Learning run from scratch using 500000 timesteps.

Overall Agent Performance:
 |Step        |Win Rate    |Av. Reward  |Av. Sep.    |
 |-1          |0.0         |0.747796462 |-0.00369498 |
 |44271       |0.0         |0.747961723 |-0.00354258 |
 |74322       |0.02        |0.767762366 |-0.00317864 |

Leading Performance:
 |Step        |Win Rate    |Av. Reward  |Av. Sep.    |
 |-1          |0.0         |0.748049433 |-0.00344866 |
 |44271       |0.0         |0.747819355 |-0.00366819 |
 |74322       |0.0         |0.748155521 |-0.00321256 |

Trailing Performance:
 |Step        |Win Rate    |Av. Reward  |Av. Sep.    |
 |-1          |0.0         |0.747543492 |-0.00394129 |
 |44271       |0.0         |0.748104091 |-0.00341697 |
 |74322       |0.04        |0.787369211 |-0.00314472 |

LEARNING LOG 

Time: 09/03/2020 12:56:23
Learning run from scratch using 500000 timesteps.

Overall Agent Performance:
 |Step        |Win Rate    |Av. Reward  |Av. Sep.    |
 |-1          |0.0         |0.747796462 |-0.00369498 |
 |44271       |0.0         |0.747961723 |-0.00354258 |
 |74322       |0.02        |0.767762366 |-0.00317864 |

Leading Performance:
 |Step        |Win Rate    |Av. Reward  |Av. Sep.    |
 |-1          |0.0         |0.748049433 |-0.00344866 |
 |44271       |0.0         |0.747819355 |-0.00366819 |
 |74322       |0.0         |0.748155521 |-0.00321256 |

Trailing Performance:
 |Step        |Win Rate    |Av. Reward  |Av. Sep.    |
 |-1          |0.0         |0.747543492 |-0.00394129 |
 |44271       |0.0         |0.748104091 |-0.00341697 |
 |74322       |0.04        |0.787369211 |-0.00314472 |

Total Wall time (mins) = 64.62261760588324
