import tensorflow as tf
tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)

import os
import gym
import gym_msr
import numpy as np
import pickle
import timeit
from stable_baselines import DQN, results_plotter
from stable_baselines.bench import Monitor
from stable_baselines.deepq.policies import MlpPolicy
from learning_tools import get_last_agent, LearningLog
import matplotlib.pyplot as plt

# Define call back function
best_av_reward, best_win_rate, eval_env, learning_log = -np.inf, -1, None, None
def callback(_locals, _globals):
    """
    Callback called at each step (for DQN an others) or after n steps (see ACER or PPO2)
    :param _locals: (dict)
    :param _globals: (dict)
    """
    global best_av_reward, best_win_rate, eval_env, learning_log

    # Evaluate after every nth reset
    repetitions = _locals['self'].env.info['repetitions']
    if repetitions % 25 == 0 and _locals['self'].env.env_steps == 0:
        print("\nEvaluating agent:\n")

        # Evaluate against all the preset policy test points.
        all_wins, all_rewards, all_separations = 0, 0, 0
        lead_wins, lead_rewards, lead_separations = 0, 0, 0
        trail_wins, trail_rewards, trail_separations = 0, 0, 0

        # Get pre-set points to test agent against (both leading and trailing)
        rider_a_lead_tests, rider_a_trail_tests = eval_env.policy_test_points()
        n_lead_tests = len(rider_a_lead_tests)
        n_trail_tests = len(rider_a_trail_tests)
        n_tests = n_lead_tests + n_trail_tests

        # Rider A is leading/Rider B trailing
        print("Rider A Leading")
        for policy_test_point in rider_a_lead_tests:
            # Evaluate the policy
            info = {}
            state = eval_env.reset(leading_rider='Rider A',
                                   policy=policy_test_point[0],
                                   policy_random_seed=policy_test_point[1],
                                   verbose=False)
            while not eval_env.complete():
                action, _ = _locals['self'].predict(state, deterministic=True)
                state, _, _, info = eval_env.step(action)

            # Store results
            all_rewards += info['total_reward']
            lead_rewards += info['total_reward']
            all_separations += (state[0] - state[3])
            lead_separations += (state[0] - state[3])
            if state[0] > state[3]:
                all_wins += 1
                lead_wins += 1
        print("Fraction of leading races won: {:.2f}\n".format(lead_wins / n_lead_tests))


        # Rider A trailing/Rider B leading
        print("Rider A Trailing")
        for policy_test_point in rider_a_trail_tests:
            # Evaluate the policy
            info = {}
            state = eval_env.reset(leading_rider='Rider B',
                                   policy=policy_test_point[0],
                                   policy_random_seed=policy_test_point[1],
                                   verbose=False)
            while not eval_env.complete():
                action, _ = _locals['self'].predict(state, deterministic=True)
                state, _, _, info = eval_env.step(action)

            # Store results
            all_rewards += info['total_reward']
            trail_rewards += info['total_reward']
            all_separations += (state[0] - state[3])
            trail_separations += (state[0] - state[3])
            if state[0] > state[3]:
                all_wins += 1
                trail_wins += 1
        print("Fraction of trailing races won: {:.2f}\n".format(trail_wins / n_trail_tests))

        # Store information in log file
        current_win_rate = all_wins / n_tests
        current_av_reward = all_rewards / n_tests

        # Save new agent when evaluated policy improves
        if current_win_rate >= best_win_rate:
            # Don't save if there is no improvement in average reward.
            if current_win_rate == best_win_rate and current_av_reward <= best_av_reward:
                # Print some results to screen
                print("Overall win rate: {:.2f}\n".format(current_win_rate))
                return True
            else:
                current_learning_step = _locals['self'].num_timesteps
                _locals['self'].save(agent_folder + '/msr_eval_' + str(current_learning_step))
                best_win_rate = current_win_rate
                best_av_reward = current_av_reward
                learning_log.add_table_line('Overall Agent Performance',
                                            [current_learning_step,
                                            current_win_rate,
                                            current_av_reward,
                                            (all_separations / n_tests)])
                learning_log.add_table_line('Leading Performance',
                                            [current_learning_step,
                                            (lead_wins / n_lead_tests),
                                            (lead_rewards / n_lead_tests),
                                            (lead_separations / n_lead_tests)])
                learning_log.add_table_line('Trailing Performance',
                                            [current_learning_step,
                                            (trail_wins / n_trail_tests),
                                            (trail_rewards / n_trail_tests),
                                            (trail_separations / n_trail_tests)])
                # Dump learning log every time new agent is saved.
                learning_log.dump()
        # Print some results to screen
        print("Overall win rate: {:.2f}\n".format(current_win_rate))
    return True


# Places to save data - folder needs to end with /
time_steps = 5000000
agent_folder = os.path.dirname(os.path.abspath(__file__)) + '/new_scratch_run/'
os.makedirs(agent_folder, exist_ok=False)
start_t = timeit.default_timer()

# Learning log
learning_log = LearningLog(agent_folder + "learning_log.txt")
learning_log.set_description("Learning run from scratch using " + str(time_steps) + " timesteps.")
learning_log.add_table('Overall Agent Performance', ['Step', 'Win Rate','Av. Reward', 'Av. Sep.'])
learning_log.add_table('Leading Performance', ['Step', 'Win Rate','Av. Reward', 'Av. Sep.'])
learning_log.add_table('Trailing Performance', ['Step', 'Win Rate','Av. Reward', 'Av. Sep.'])

# Create random sprint environment with a monitor
env = gym.make('msr-duel-fixed-v1')
eval_env = gym.make('msr-duel-fixed-v1')
monitor_file = agent_folder + 'monitor_log'
env = Monitor(env, monitor_file, allow_early_resets=True)

# Create agent with hyperparams & train.
model = DQN(MlpPolicy, env, gamma=0.99, buffer_size=250000, batch_size=256, exploration_fraction=0.12952,
            target_network_update_freq=500, prioritized_replay=False, verbose=0)
model.learn(total_timesteps=time_steps, callback=callback)

# Visualise training results
results_plotter.plot_results([agent_folder], time_steps, results_plotter.X_TIMESTEPS, "Duelling Environment")
plt.savefig(agent_folder + '/learning_plot.png')

# Process time values
process_time = (timeit.default_timer() - start_t)/60
learning_log.add_line("Total Wall time (mins) = " + str(process_time))

# Report and save log data
learning_log.report()
learning_log.dump()
with open(agent_folder + 'learning_log.data', 'wb') as handle:
    pickle.dump(learning_log, handle)