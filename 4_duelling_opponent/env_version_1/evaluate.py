import tensorflow as tf
tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)

import os
import gym
from stable_baselines import DQN
from learning_tools import plot_action_separation

# Load trained model
eval_folder = os.path.dirname(os.path.abspath(__file__)) + "/space_5/scratch_1/evaluate/"
agent_path = eval_folder + 'msr_eval'

# Environment and model
env = gym.make('msr-duel-fixed-v1')
model = DQN.load(agent_path)
model.set_env(env)

# Get pre-set points to test agent against (both leading and trailing)
rider_a_lead_tests, rider_a_trail_tests = env.policy_test_points()
n_lead_tests = len(rider_a_lead_tests)
n_trail_tests = len(rider_a_trail_tests)
n_tests = n_lead_tests + n_trail_tests

# Rider A is leading/Rider B trailing
print("Rider A Leading")
for policy_test_point in rider_a_lead_tests:
    # Evaluate the policy
    info = {}
    state = env.reset(leading_rider='Rider A',
                        policy=policy_test_point[0],
                        policy_random_seed=policy_test_point[1],
                        verbose=True)
    while not env.complete():
        action, _ = model.predict(state, deterministic=True)
        state, _, _, info = env.step(action)

    # Save history
    hist_save_name = eval_folder + "rider_a_lead_" + str(policy_test_point[1])
    env.save_history(name=hist_save_name)

    # Plot & save A/S
    plot_action_separation(histories=[hist_save_name],
                        titles=['Rider A Leading, Policy Seed ' + str(policy_test_point[1])],
                        single_althete=False,
                        save=True,
                        image_save_name=eval_folder + "rider_a_lead_" + str(policy_test_point[1]),
                        show=False)

# Rider A trailing/Rider B leading
print("Rider A Trailing")
for policy_test_point in rider_a_trail_tests:
    # Evaluate the policy
    info = {}
    state = env.reset(leading_rider='Rider B',
                        policy=policy_test_point[0],
                        policy_random_seed=policy_test_point[1],
                        verbose=True)
    while not env.complete():
        action, _ = model.predict(state, deterministic=True)
        state, _, _, info = env.step(action)

    # Save history
    hist_save_name = eval_folder + "rider_a_trail_" + str(policy_test_point[1])
    env.save_history(name=hist_save_name)

    # Plot & save A/S
    plot_action_separation(histories=[hist_save_name],
                        titles=['Rider A Trailing, Policy Seed ' + str(policy_test_point[1])],
                        single_althete=False,
                        save=True,
                        image_save_name= eval_folder + "rider_a_trail_" + str(policy_test_point[1]),
                        show=False)