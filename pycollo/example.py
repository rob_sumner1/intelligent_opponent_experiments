import numpy as np
import sympy as sym

import pycollo


from timeit import default_timer as timer

import matplotlib.pyplot as plt
import numpy as np
import sympy as sym
import sympy.physics.mechanics as me

import lapysim

vittoria_cl_gold_160_psi = lapysim.Tyre.from_library('vittoria cl gold')
mavic_ellipse_700_cc = lapysim.Wheel.from_library('mavic ellipse',
    tyre=vittoria_cl_gold_160_psi)
cervelo_T5GB_size_M = lapysim.Bike.from_library('cervelo t5gb',
    frame_size='M', front_wheel=mavic_ellipse_700_cc,
    rear_wheel=mavic_ellipse_700_cc)

rider_a_anthropometry = lapysim.Anthropometry(mass=80, drag_area=0.26,
    height=1.82)
rider_a_physiology = lapysim.VariableFatigueMaximalSubmaximalModel()
rider_a_bike_fit = lapysim.BikeFit(saddle_height=1.1, gear_ratio=3.8)
rider_a = lapysim.Athlete(
    name='Rider A',
    athlete_id='rdrA',
    bike=cervelo_T5GB_size_M,
    anthropometry=rider_a_anthropometry,
    physiology=rider_a_physiology,
    bike_fit=rider_a_bike_fit,
    )

match_sprint_conditions = lapysim.Conditions()
match_sprint_track = lapysim.Manchester()
match_sprint_venue = lapysim.Venue(course=match_sprint_track,
    conditions=match_sprint_conditions)
match_sprint_rules = lapysim.Rules()

match_sprint_event = lapysim.Event(
    venue=match_sprint_venue,
    athletes=(rider_a, ),
    rules=match_sprint_rules,
    )

match_sprint_simulation = match_sprint_event.initialise_forward_simulation()

problem = pycollo.OptimalControlProblem(
    state_variables= match_sprint_event._athletes[0]._state_syms,
    control_variables= match_sprint_event._athletes[0]._control_syms
    )

problem.state_equations = match_sprint_event._athletes[0]._dynamics_equations

problem.objective_function = problem.final_time
# problem.final_state[0] - problem.final_state[3]

problem.state_endpoint_constraints = [problem.initial_state[0],
    problem.initial_state[1],
    problem.initial_state[2],
	problem.final_state[0]]

problem.bounds = pycollo.Bounds(optimal_control_problem=problem,
    initial_time_lower=0.0,
    initial_time_upper=0.0,
    final_time_lower=0.0,
    final_time_upper=100.0,
    state=[[0, 250], [0, 100], [0, 1]],
    control_lower=0,
    control_upper=1,
    state_endpoint_lower=[0, 0, 1, 250],
    state_endpoint_upper=[0, 0, 1, 250])

# Guess
problem.initial_guess = pycollo.Guess(optimal_control_problem=problem,
    time=[0.0, 20.0],
    state=np.array([[0, 250], [0, 10], [1, 0.1]]),
    control=np.array([1.0, 1.0]),
    state_endpoints_override=True)

problem.auxiliary_data = match_sprint_event._athletes[0]._auxiliary_mappings
# problem.auxiliary_data = {**match_sprint_event._athletes[0]._auxiliary_mappings, {match_sprint_event._athletes[1]._control_syms[0]: 1}}
print(problem.auxiliary_data)

problem.settings.display_mesh_result_graph = True

# problem.solve()