# Intelligent Opponent Experiments

Set of experiments into an intelligent opponent for the Match Sprint Race, conducted as part of a final year project.

## Getting Started

This repository represents a record of experiments performed as part of the project. 
The repo is structured to roughly follow the order of experiments (with numbered folders indicating the order of events). 
Where possible, *description.txt* files have been added to indicate the nature of an experiment or result. 

### Prerequisites

To repeat experiments in this repo, follow the instructions given in *setup.txt* for creating a virtual environment. 
* These instructions worked as of April 2020. 
* Some memory issues have been experienced, especially when using Stable-Baselines but this was found to be due to package conflicts. Following the instructions should prevent this. 

Several of the files in this folder require significant computation power. The user should consider employing cloud computing, or at least reduce the training steps before you are happy with run times. 

## Authors
* **Rob Sumner** (Final year project work). 

## License
This project is copywrited - see LICENSE.md for more details. 
