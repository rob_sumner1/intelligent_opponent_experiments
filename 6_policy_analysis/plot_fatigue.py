from learning_tools import plot_fatigue_analysis


folders = ['continue_d/', 'expert/', 'pretrain_5/', 'pretrain_continue_a/']
titles = ['Agent Trained from Scratch', 
  	  'Expert Policy', 
	  'Pretrained Agent', 
 	  'Pretrained Agent with Continuation Training']

for i, folder in enumerate(folders):
   plot_fatigue_analysis(folder + 'overall_win_rate.npy', 
			   [0.009,0.0115], title='Overall Win Rate: '+titles[i], 
			   save_name=folder+'overall_fatigue_plot', 
			   show=False)
   plot_fatigue_analysis(folder + 'lead_win_rate.npy', 
			   [0.009,0.0115], title='Leading Win Rate: '+titles[i], 
			   save_name=folder+'leading_fatigue_plot', 
			   show=False)
   plot_fatigue_analysis(folder + 'trail_win_rate.npy', 
			   [0.009,0.0115], title='Trailing Win Rate: '+titles[i], 
			   save_name=folder+'trailing_fatigue_plot', 
			   show=False)


