import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
import tensorflow as tf
tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)
import os
os.environ["KMP_WARNINGS"] = "FALSE"

import gym
from stable_baselines import DQN
from gym_msr.envs.msr_policies import MsrPolicy
from learning_tools import analyse_policies, plot_policy_analysis, plot_action_separation, measure_win_rate
import matplotlib.pyplot as plt
import numpy as np

# Agent a file save locations
agent_folder = os.path.dirname(os.path.abspath(__file__)) + '/expert/'
agent_file = "size_5_new"
os.makedirs(agent_folder, exist_ok=True)
save_name = agent_folder + 'full_policy_analysis_' + agent_file

# Set/Load agent
#agent_filepath = agent_folder + agent_file + '.zip'
#rider_a_agent = DQN.load(agent_filepath)
rider_a_agent = MsrPolicy(rider='Rider A')

# # Run analysis of policy.
results = analyse_policies(rider_a_agent, n_steps=100, n_step_runs=75, 
				save_name=save_name, action_dim=5)
plot_policy_analysis([save_name], labels=['Expert $|A|=5$'], 
                      show=True, legend_loc='lower right',
                      save_name=agent_folder+'policy_plot_test_run')
