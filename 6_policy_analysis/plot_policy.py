from learning_tools import plot_policy_analysis


expert_5 = 'expert/full_policy_analysis_size_5.npy'
expert_200 = 'expert/full_policy_analysis_size_200.npy'
folders = ['continue_d/', 'pretrain_5/', 'pretrain_continue_a/']
labels = ['Agent Trained from Scratch', 
	  'Pretrained Agent', 
 	  'Pretrained Agent with Continuation Training']

for i, folder in enumerate(folders):
   policy_file = folder + 'full_policy_analysis.npy'
   plot_policy_analysis([expert_5, policy_file], 
			labels=['Expert Policy $|A|=5$', labels[i]],
	 		save_name=folder+'policy_expert_5')
   plot_policy_analysis([expert_200, policy_file], 
			labels=['Expert Policy $|A|=200$', labels[i]],
	 		save_name=folder+'policy_expert_200')

# Plot expert policies on their own
plot_policy_analysis([expert_5, expert_200], 
			labels=['Expert Policy $|A|=5$', 'Expert Policy $|A|=200$'],
	 		save_name='expert/policy_plot')



