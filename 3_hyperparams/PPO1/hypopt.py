import tensorflow as tf
tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)

import os
import glob
import gym
import pickle
import timeit
import optuna
import pandas
import numpy as np
from learning_tools import LearningLog
from stable_baselines.bench import Monitor
from stable_baselines.common.policies import MlpPolicy
from stable_baselines import PPO1

# Global variables used for callback.
best_win_rate, best_av_sep = 0, -np.inf

# Define callback function
# This is used to evaluate learning progress during training.
def callback(_locals, _globals):
    """
    Callback called after every 30 learning races.
    :param _locals: (dict)
    :param _globals: (dict)
    """

    # Need to use global variables.
    global best_win_rate, best_av_sep

    # Evaluate after every 30th reset
    repetitions = _locals['self'].env.info['repetitions']
    if repetitions % 30 == 0 and _locals['self'].env.env_steps == 0:
        # Give evaluation details
        # print('Evaluating agent after '+str(repetitions)+' training races.')

        # Evaluate test env on different distances.
        distances = np.linspace(100,500,7)
        evaluated_wins = 0
        total_sep = 0

        for distance in distances:
            state = _locals['self'].env.reset(distance=distance, verbose=False)
            while not _locals['self'].env.complete():
                action, _ = _locals['self'].predict(state, deterministic=True)
                state, _, _, _ = _locals['self'].env.step(action)
            total_sep += (state[0] - state[3])
            if state[0] > state[3]:
                evaluated_wins += 1

        # Reset number of learning repetitions
        _locals['self'].env.reset(verbose=False)
        _locals['self'].env.info['repetitions'] = repetitions

        # Store if more races won
        # If race number is same, save if average separation is higher.
        current_win_rate = evaluated_wins/len(distances)
        current_av_sep = total_sep / len(distances)

        if current_win_rate > best_win_rate:
            best_win_rate = current_win_rate
            best_av_sep = current_av_sep
        elif current_win_rate == best_win_rate and current_av_sep > best_av_sep:
            best_av_sep = current_av_sep

        # # Print some results to screen
        # print("Fraction of races won: {:.2f}".format(current_win_rate))
        # print("Average separation over races: {:.2f}".format(current_av_sep))
    return True

class Objective(object):
    def __init__(self, env, n_runs=5, n_steps=100000):
        """Initialise objective with passable arguments."""
        self.env = env
        self.n_runs = n_runs
        self.n_steps = n_steps

    def __call__(self, trial):
        """Objective function to use in optuna optimisation study."""

        # Need to use global variables again here
        global best_win_rate, best_av_sep

        # Set hyperparameter values
        discount_factor = 0.99
        clip_param = trial.suggest_uniform('clip_eta', 0.1, 0.3)
        optim_epochs = trial.suggest_categorical('optim_epochs', [2, 4, 8])
        optim_step = trial.suggest_uniform('optim_step', 0.00025, 0.001)
        optim_batch = trial.suggest_categorical('optim_batch', [32, 64, 128])

        # Setup folder for log file.
        folder = os.path.dirname(os.path.abspath(__file__)) + '/logs/'
        os.makedirs(folder, exist_ok=True)
        start_t = timeit.default_timer()
        last_t = start_t

        # Create the Learning log file to maintain run counting.
        log_number = len(glob.glob1(folder,"*.txt")) + 1
        log_filepath = folder + "objlog_" + str(log_number) + ".txt"
        with open(log_filepath, 'a'):
            os.utime(log_filepath, None)

        # Learning log tracks objective function call data.
        log = LearningLog(log_filepath)
        log.set_description("Set of learning runs, each corresponding to " + \
                            str(self.n_runs) + " runs of " + str(self.n_steps) + \
                            " timesteps on action space with 15 elements," + \
                            " using scaled distance reward mode.")
        log.add_table('Performance table',
                    ['Run', 'Best Win Rate', 'Best Av. Sep.', 'Walltime'])

        # Random seeds set for each run
        seeds = [6981745, 2511124, 7007879, 2737331, 9798007, 8077833, 7255044]

        win_rates, sep_vals = [], []
        for run in range(self.n_runs):
            # Reset values
            print('\nLearning run = ' + str(run))
            best_win_rate, best_av_sep = -1, -np.inf
            self.env.reset(verbose=False)
            self.env.info['repetitions'] = 0

            # Create random sprint environment with a monitor
            monitor_env = Monitor(self.env, folder + 'objcall_' + str(log_number) , allow_early_resets=True)

            # Create and train agent.
            model = PPO1(MlpPolicy, monitor_env, verbose=0, gamma=discount_factor,
                        clip_param=clip_param, optim_epochs=optim_epochs,
                        optim_batchsize=optim_batch, optim_stepsize=optim_step,
                        seed=seeds[run])
            model.learn(total_timesteps=self.n_steps, callback=callback)
            print('Best win rate: ', best_win_rate)
            print('Best average sep: ', best_av_sep)

            # Store results in log
            win_rates.append(best_win_rate)
            sep_vals.append(best_av_sep)
            log.add_table_line('Performance table',
                                [run+1, best_win_rate, best_av_sep,
                                (timeit.default_timer()-last_t)/60])
            last_t = timeit.default_timer()

            # Calculate intermediate objective function for pruning
            intermediate_win_rate = np.mean(win_rates)
            if intermediate_win_rate == 1:
                intermediate_objval = intermediate_win_rate + np.mean(sep_vals)
            else:
                intermediate_objval = intermediate_win_rate

            # Handle pruning based on the intermediate value.
            trial.report(intermediate_objval, run)
            if trial.should_prune():
                raise optuna.exceptions.TrialPruned()

        # Add hyperparameters to log file
        log.add_line('Hyperparameters used: ')
        log.add_line('Discount factor = ' + str(discount_factor))
        log.add_line('Clipping parameter eta = ' + str(clip_param))
        log.add_line('Optimiser epochs = ' + str(optim_epochs))
        log.add_line('Optimiser step size = ' + str(optim_step))
        log.add_line('Optimiser batch size = ' + str(optim_batch) + '\n')

        # Complete & dump log file.
        log.add_line('Average values:')
        average_win_rate = np.mean(win_rates)
        average_sep_val = np.mean(sep_vals)
        log.add_line("Average Win rate across runs = " + str(average_win_rate))
        log.add_line("Average Separation across runs = " + str(average_sep_val))
        process_time = (timeit.default_timer() - start_t)/60
        av_run_time = process_time / self.n_runs
        log.add_line("Average Wall time per run (mins) = " + str(av_run_time))
        log.report()
        log.dump()
        with open(folder + "objlog_" + str(log_number) + '.data', 'wb') as handle:
            pickle.dump(log, handle)

        # Return mean win rate as objective value
        if average_win_rate == 1:
            return average_win_rate + average_sep_val
        else:
            return average_win_rate

# Create single environments to use (saves memory)
env = gym.make('msr-submax-sprint-v1',
                    action_dim=5,
                    reward_mode='scaled_distance',
                    verbose=False)

# Create study object and run using single env.
study = optuna.create_study(study_name='ppo1_search',
                            storage='sqlite:///ppo1_search.db',
                            load_if_exists=True,
                            direction='maximize',
                            pruner=optuna.pruners.PercentilePruner(50.0))
study.optimize(Objective(env, n_runs=4, n_steps=150000), n_trials=1)
df = study.trials_dataframe(attrs=('number', 'value', 'params', 'state'))
df.to_csv(os.path.dirname(os.path.abspath(__file__)) + '/study_results.csv')
