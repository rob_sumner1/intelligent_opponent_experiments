import os
import gym
import pickle
import numpy as np
from stable_baselines import DQN
# from stable_baselines.deepq.policies import MlpPolicy
from learning_tools import run_model, plot_action_separation, plot_fatigue, LearningLog
import matplotlib.pyplot as plt

# Load trained model
action_dim = 9
# eval_folder = os.path.dirname(os.path.abspath(__file__)) +  \
#     '/agents/experiment_4/separation/run_4/evaluation/'
# agent_path = eval_folder + 'msr_final'
eval_folder = os.path.dirname(os.path.abspath(__file__)) +  \
    '/agents/experiment_4/space_' + str(action_dim) + '_multiple/run_5/evaluate/'
agent_path = eval_folder + 'msr_eval'

# Setup log
log = LearningLog(eval_folder + "results.txt")
log.set_description("Evaluation of trained model")
log.add_table('Results Table', ['Sprint Distance',
               'Reward',
               'Final Sep.',
               'Win?'])

# Range of distances to test
distances = [100, 150, 200, 250, 300, 350, 400, 450, 500]
# distances = np.linspace(100, 500, num=40) #10 m increments.

# Environment and model
env = gym.make('msr-submax-sprint-v1', action_dim=action_dim, time_step=0.3)
model = DQN.load(agent_path)
model.set_env(env)

# Run model for all distances
total_sep = 0
for distance in distances:
    # Get reward and save histories
    obs = env.reset(distance=distance)
    hist_name = eval_folder + "hist_" + str(int(distance))
    reward = run_model(env, model, obs=obs, hist_save_name=hist_name)

    # Get separation
    hist_file = open(hist_name + ".hist", 'rb')
    data = pickle.load(hist_file)
    hist_file.close()
    sep = data[1]['state'][-1][0] - data[2]['state'][-1][0]
    total_sep += sep
    win = "N"
    if sep > 0:
        win = "Y"

    # Store data
    log.add_table_line('Results Table', [env.race_distance - distance, reward, sep, win])

# Add average sep line
log.add_line("Average separation value = " + str(total_sep/len(distances)))

# Save data
# data_file =open(eval_folder + "results.txt",'w')
# data_file.write("Sprint Distance - Rewards: \n")
# for i, reward in enumerate(rewards):
#     data_file.write("%s - %s\n" % (env.race_distance - distances[i], reward))
# data_file.write("\nSprint Distance - Separation values: \n")
# for i, sep in enumerate(sep_vals):
#     data_file.write("%s - %s\n" % (env.race_distance - distances[i], sep))
# data_file.close()
log.report()
log.dump()
with open(eval_folder + 'analysis_log.data', 'wb') as handle:
    pickle.dump(log, handle)

# # Plot results
plot_action_separation(folder=eval_folder, save=True)
plot_fatigue(folder=eval_folder, save=True)