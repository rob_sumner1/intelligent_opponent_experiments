import os
import gym
import gym_msr
import numpy as np
import timeit
from stable_baselines import DQN, results_plotter
from stable_baselines.bench import Monitor
from stable_baselines.deepq.policies import MlpPolicy
from stable_baselines.results_plotter import load_results, ts2xy
from learning_tools import get_last_agent, LearningLog
import matplotlib.pyplot as plt

# Define call back function
best_mean_reward, n_steps = -np.inf, -1
best_mean_eval_reward, eval_wins, eval_win_step = -np.inf, -1, -1
def callback(_locals, _globals):
    """
    Callback called at each step (for DQN an others) or after n steps (see ACER or PPO2)
    :param _locals: (dict)
    :param _globals: (dict)
    """
    global n_steps, best_mean_reward, eval_wins, eval_win_step, best_mean_eval_reward

    # Evaluate after every nth reset
    repetitions = _locals['self'].env.info['repetitions']
    if repetitions % 10 == 0 and _locals['self'].env.env_steps == 0:
        print("\nEvaluating agent:")

        # Save based on 1000 step best mean reward
        x, y = ts2xy(load_results(log_dir), 'timesteps')
        if len(x) > 0:
            mean_reward = np.mean(y[-1000:])
            print(x[-1], 'timesteps')
            print("Best mean reward: {:.2f} - Current 1000 step mean reward: {:.2f}".format(best_mean_reward, mean_reward))
            if mean_reward > best_mean_reward:
                best_mean_reward = mean_reward
                # _locals['self'].save(agent_folder + '/msr_callback_' + str(n_steps))

        # Evaluate test env on different distances.
        distances = [100, 150, 200, 250, 300, 350, 400, 450, 500]
        evaluated_wins = 0
        total_rewards = 0
        eval_env = gym.make('msr-submax-sprint-v1', action_dim=action_dim)
        for distance in distances:
            # Get reward and save histories
            state = eval_env.reset(distance=distance, verbose=False)
            info = {}
            while not eval_env.complete():
                action, _ = _locals['self'].predict(state, deterministic=True)
                state, _, _, info = eval_env.step(action)
            total_rewards += info['total_reward']
            if state[0] > state[3]:
                evaluated_wins += 1
        av_reward = total_rewards / len(distances)

        # Store if more races won
        # If race number is same, save if average reward is higher.
        if evaluated_wins > eval_wins:
            _locals['self'].save(agent_folder + '/msr_eval_' + str(n_steps))
            eval_wins = evaluated_wins
            best_mean_eval_reward = av_reward
            eval_win_step = n_steps
        elif evaluated_wins == eval_wins and av_reward > best_mean_eval_reward:
            _locals['self'].save(agent_folder + '/msr_eval_' + str(n_steps))
            best_mean_eval_reward = av_reward
            eval_win_step = n_steps

        # Print some results to screen
        print("Fraction of races won: {:.2f} - Average reward over races: {:.2f}\n".format(evaluated_wins/len(distances), av_reward))

    n_steps += 1
    return True

# Folder location for learning
# Needs to end with a '/'
n_runs = 5
action_dim = 11
time_steps = 50000
folder = os.path.dirname(os.path.abspath(__file__)) + '/agents/experiment_3/space_11_continue/'
agent = folder + 'msr_init'
if not os.path.exists(folder) or not os.path.exists(agent + ".zip"):
    raise RuntimeError("Folder or agent does not exist.")

# Learning log
log = LearningLog(folder + "best_rewards.txt")
log.set_description("Continuation of learning run, each corresponding to " +
			        str(time_steps) + " timesteps on action space with " + \
                    str(action_dim) + " elements.")
log.set_table(['Repeat',
               'Best av. R',
               'Best # wins',
               'Best av. eval. R',
               'Step',
               'Walltime'])

# Load agent
model = DQN.load(agent)
env = gym.make('msr-submax-sprint-v1', action_dim=action_dim)

# Get initial callback values to compare to
distances = [100, 150, 200, 250, 300, 350, 400, 450, 500]
evaluated_wins, total_rewards = 0, 0
for distance in distances:
    # Get reward and save histories
    state = env.reset(distance=distance, verbose=False)
    info = {}
    while not env.complete():
        action, _ = model.predict(state, deterministic=True)
        state, _, _, info = env.step(action)
    total_rewards += info['total_reward']
    if state[0] > state[3]:
        evaluated_wins += 1
av_reward = total_rewards / len(distances)
log.add_line("Initial agent values: Wins = " + str(evaluated_wins) + ", Av. reward = " + str(av_reward))

# Training runs.
start_t = timeit.default_timer()
last_t = start_t
best_rewards = []
start_t = timeit.default_timer()

for i in range(n_runs):
    # Re load agent
    print('\nLearning run = ' + str(i) + '\n')
    model = DQN.load(agent)

    # Reset values
    best_mean_reward, n_steps = -np.inf, -1
    best_mean_eval_reward, eval_wins = av_reward, evaluated_wins

    # Setup experiment and log folder and save directory
    agent_folder = folder + "run_" + str(i+1)
    os.makedirs(agent_folder, exist_ok=True)
    log_dir = agent_folder + '/log'
    os.makedirs(log_dir, exist_ok=True)

    # Create random sprint environment with a monitor
    env = gym.make('msr-submax-sprint-v1', action_dim=action_dim)
    env = Monitor(env, log_dir, allow_early_resets=True)

    # Train agent
    model.set_env(env)
    model.learn(total_timesteps=time_steps, callback=callback, reset_num_timesteps=False)
    best_rewards.append(best_mean_reward)

    # Store results in log
    log.add_table_line([i+1, best_mean_reward, eval_wins,
                        best_mean_eval_reward, eval_win_step,
                        (timeit.default_timer() - last_t)/60])
    last_t = timeit.default_timer()

    # Visualise training results
    results_plotter.plot_results([log_dir], time_steps, results_plotter.X_TIMESTEPS, "Random Sprint")
    plt.savefig(agent_folder + '/learning_plot.png')

# Process time values
process_time = (timeit.default_timer() - start_t)/60
av_run_time = process_time / n_runs
log.add_line("Total Wall time (mins) = " + str(process_time))
log.add_line("Average Wall time per run (mins) = " + str(av_run_time))

# Report and save log data
log.report()
log.dump()
