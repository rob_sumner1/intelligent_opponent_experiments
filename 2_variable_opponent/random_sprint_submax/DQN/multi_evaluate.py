import tensorflow as tf
tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)

import os
import gym
import pickle
import numpy as np
from stable_baselines import DQN
# # from stable_baselines.deepq.policies import MlpPolicy
from learning_tools import run_model, get_agents, LearningLog
# import matplotlib.pyplot as plt

def evaluate(model_filename, env, n_dist=41):
    """Evaluate a given model.
        Parameters:
        n_dist - Number of distance increments to use.
        Returns:
        win_rate - The model's win rate.
    """
    # Distances to trial over
    distances = np.linspace(100, 500, num=n_dist)

    # Create folder path
    eval_folder = '/'.join(model_filename.split('/')[0:-1]) + '/'
    agent_name = model_filename.split('/')[-1]

    # Create a folder for the history files
    hist_folder = eval_folder + agent_name + "_histories/"
    if not os.path.isdir(hist_folder):
        os.mkdir(hist_folder)

    # Create log file for single agent.
    log = LearningLog(hist_folder + 'agent_analysis.txt')
    log.set_description("Evaluation of model defined in " + agent_name)
    log.add_table('Sprint Distance Table',
                ['Sprint Distance',
                'Reward',
                'Final Sep.',
                'Win?'])

    # Load the model
    model = DQN.load(model_filename)
    model.set_env(env)

    # Run model for all distances
    total_sep = 0
    total_wins = 0
    for distance in distances:
        # Get reward and save histories
        obs = env.reset(distance=distance)
        hist_name = hist_folder + "hist_" + str(int(distance))
        reward = run_model(env, model, obs=obs, hist_save_name=hist_name)

        # Get separation
        hist_file = open(hist_name + ".hist", 'rb')
        data = pickle.load(hist_file)
        hist_file.close()
        sep = data[1]['state'][-1][0] - data[2]['state'][-1][0]
        total_sep += sep
        win = "N"
        if sep > 0:
            win = "Y"
            total_wins += 1

        # Store data
        log.add_table_line('Sprint Distance Table', [env.race_distance - distance, reward, sep, win])

    # Add average sep line
    win_rate = total_wins/len(distances)
    avg_sep = total_sep/len(distances)
    log.add_line("Win rate = " + str(win_rate))
    log.add_line("Average separation value = " + str(avg_sep))

    # Save out the log file
    log.dump()
    with open(hist_folder + 'analysis_log.data', 'wb') as handle:
        pickle.dump(log, handle)

    return win_rate, avg_sep

# Setup
action_dim = 3
reward_mode = "scaled_distance" # 'scaled_distance' or 'separation'
env = gym.make('msr-submax-sprint-v1', action_dim=action_dim, reward_mode=reward_mode)
eval_folder = os.path.dirname(os.path.abspath(__file__)) +  \
    '/agents/experiment_4/space_' + str(action_dim) + '_multiple/'
max_steps = [50000, 75000, 100000, 125000, 150000]

# Create overall log file definition
log = LearningLog(eval_folder + "analysis.txt")
log.set_description("Analysing set of learning runs, each corresponding " + \
                    "to varying number of timesteps " + \
                    " on action space with " + str(action_dim) + \
                    " elements, using " + reward_mode + " reward mode.")
log.add_table('Win Rate Comparison table',
              ['Run'] + [str(val) for val in max_steps])
log.add_table('Average Separation Comparison table',
              ['Run'] + [str(val) for val in max_steps])
log.add_table('Averaged Win Rate Comparison',
              ['# Runs Used'] + [str(val) for val in max_steps])

# Read in timesteps from result file and decide which need to be
# evaluated.
start_line, end_line = 18, 24
data = open(eval_folder + 'best_rewards.txt', 'r').readlines()
time_step_array = np.ndarray((end_line-start_line+1, 5))
for run_index in range(7):
    l = data[run_index + start_line - 1].split('|')
    agent_steps = []
    for j in range(6): agent_steps.append(int(l[j+2]))
    for max_step_index, max_step in enumerate(max_steps):
        for agent_step in agent_steps:
            if agent_step <= max_step:
                time_step_array[run_index, max_step_index] = agent_step
            else: break

# Get results for each run.
win_rates = np.zeros((7, 5))
avg_seps = np.zeros(win_rates.shape)
for run_index in range(7):
    for step in np.unique(time_step_array[run_index,:]):
        agent_file = eval_folder + "run_" + str(run_index+1) + "/msr_eval_" + str(int(step))
        win_rate, avg_sep = evaluate(agent_file, env)
        win_rates[run_index, np.where(time_step_array[run_index,:] == step)] = win_rate
        avg_seps[run_index, np.where(time_step_array[run_index,:] == step)] = avg_sep
print(win_rates)
print(avg_seps)

# Average over win rates for number of steps.
avg_win_rates = np.zeros((7, 5))
for n_runs in range(7):
    avg_win_rates[n_runs,:] = np.mean(win_rates[:n_runs+1, :], axis=0)


# Store results in log file
for run_index in range(7):
    log.add_table_line('Win Rate Comparison table',
                        [run_index+1] + [win for win in win_rates[run_index,:]])
    log.add_table_line('Average Separation Comparison table',
                        [run_index+1] + [sep for sep in avg_seps[run_index,:]])
    log.add_table_line('Averaged Win Rate Comparison',
                        [run_index + 1] + [avg_win for avg_win in avg_win_rates[run_index, :]])
log.report()
log.dump()
with open(eval_folder + 'analysis_log.data', 'wb') as handle:
    pickle.dump(log, handle)
