import os
import gym
import gym_msr
import numpy as np
import pickle
import timeit
from stable_baselines import DQN, results_plotter
from stable_baselines.bench import Monitor
from stable_baselines.deepq.policies import MlpPolicy
from stable_baselines.results_plotter import load_results, ts2xy
from learning_tools import get_last_agent, LearningLog
import matplotlib.pyplot as plt

# Define call back function
saved_model_data = []
best_mean_reward, n_steps = -np.inf, -1
best_mean_eval_reward, eval_win_rate, eval_win_step = -np.inf, -1, -1
def callback(_locals, _globals):
    """
    Callback called at each step (for DQN an others) or after n steps (see ACER or PPO2)
    :param _locals: (dict)
    :param _globals: (dict)
    """
    global n_steps, best_mean_reward, best_win_rate, best_win_step, best_mean_eval_reward

    # Evaluate after every nth reset
    repetitions = _locals['self'].env.info['repetitions']
    if repetitions % 20 == 0 and _locals['self'].env.env_steps == 0:
        print("\nEvaluating agent:")

        # Save based on 1000 step best mean reward
        x, y = ts2xy(load_results(log_dir), 'timesteps')
        if len(x) > 0:
            current_mean_reward = np.mean(y[-1000:])
            print(x[-1], 'timesteps')
            print("Best mean reward: {:.2f} - Current 1000 step mean reward: {:.2f}".format(best_mean_reward, current_mean_reward))
            if current_mean_reward > best_mean_reward:
                best_mean_reward = current_mean_reward
                # _locals['self'].save(agent_folder + '/msr_callback_' + str(n_steps))

        # Evaluate test env on different distances.
        distances = [100, 200, 300, 400, 500]
        evaluated_wins = 0
        total_rewards = 0
        eval_env = gym.make('msr-submax-sprint-v1', action_dim=action_dim, reward_mode=reward_mode)
        for distance in distances:
            # Get reward and save histories
            state = eval_env.reset(distance=distance, verbose=False)
            info = {}
            while not eval_env.complete():
                action, _ = _locals['self'].predict(state, deterministic=True)
                state, _, _, info = eval_env.step(action)
            total_rewards += info['total_reward']
            if state[0] > state[3]:
                evaluated_wins += 1
        current_win_rate = evaluated_wins/len(distances)
        av_reward = total_rewards / len(distances)

        # Store if more races won
        # If race number is same, save if average reward is higher.
        if current_win_rate > best_win_rate:
            _locals['self'].save(agent_folder + '/msr_eval_' + str(n_steps))
            best_win_rate = current_win_rate
            best_mean_eval_reward = av_reward
            best_win_step = n_steps
            saved_model_data.append((best_mean_reward, best_win_rate,
                        best_mean_eval_reward, best_win_step))
        elif current_win_rate == best_win_rate and av_reward > best_mean_eval_reward:
            _locals['self'].save(agent_folder + '/msr_eval_' + str(n_steps))
            best_mean_eval_reward = av_reward
            best_win_step = n_steps
            saved_model_data.append((best_mean_reward, best_win_rate,
                        best_mean_eval_reward, best_win_step))

        # Print some results to screen
        print("Fraction of races won: {:.2f} - Average reward over races: {:.2f}\n".format(evaluated_wins/len(distances), av_reward))

    n_steps += 1
    return True

# Places to save data
# folder needs to end with /
action_dim = 15
n_runs = 7
reward_mode = "separation" # 'scaled_distance' or 'separation'
time_steps = 150000
folder = os.path.dirname(os.path.abspath(__file__)) + '/agents/experiment_2/separation/'
os.makedirs(folder, exist_ok=False)
start_t = timeit.default_timer()
last_t = start_t

# Learning log
log = LearningLog(folder + "best_rewards.txt")
log.set_description("Set of learning runs, each corresponding to " +
			        str(time_steps) + " timesteps on action space with " + \
                    str(action_dim) + " elements, using " + reward_mode + " reward mode.")
log.add_table('Final performance table',
              ['Run', 'Best av. R', 'Best Win Rate',
               'Best av. eval. R', 'Step', 'Walltime'])
step_increments = [25000, 50000, 75000, 100000, 125000, 150000]
log.add_table('Run / Timestep win rates',
              ['Run'] + [str(val) for val in step_increments])
log.add_table('Average over runs', ['Number of runs', 'Average Win Rate'])

win_rates = []
for run in range(n_runs):
    # Reset values
    print('\nLearning run = ' + str(run) + '\n')
    saved_model_data = []
    best_mean_reward, n_steps = -np.inf, -1
    best_mean_eval_reward, best_win_rate, best_win_step = -np.inf, -1, -1

    # Setup experiment and log folder and save directory
    agent_folder = folder + "run_" + str(run+1)
    os.makedirs(agent_folder, exist_ok=True)
    log_dir = agent_folder + '/log'
    os.makedirs(log_dir, exist_ok=True)

    # Create random sprint environment with a monitor
    env = gym.make('msr-submax-sprint-v1', action_dim=action_dim, reward_mode=reward_mode)
    env = Monitor(env, log_dir, allow_early_resets=True)

    # Create and train agent.
    model = DQN(MlpPolicy, env, verbose=0)
    model.learn(total_timesteps=time_steps, callback=callback)

    # Separate out saved models by timestep
    step_data = [run+1]
    for step in step_increments:
        step_best_win_rate = 0
        for j in range(len(saved_model_data)):
            if saved_model_data[j][3] < step:
                step_best_win_rate = saved_model_data[j][3]
            else:
                break
        step_data += [step_best_win_rate]

    # Store results in log
    win_rates.append(best_win_rate)
    log.add_table_line('Final performance table',
                        [run+1, best_mean_reward, best_win_rate,
                         best_mean_eval_reward, best_win_step,
                         (timeit.default_timer() - last_t)/60])
    log.add_table_line('Run / Timestep win rates', step_data)
    last_t = timeit.default_timer()

    # Visualise training results
    results_plotter.plot_results([log_dir], time_steps, results_plotter.X_TIMESTEPS, "Random Sprint")
    plt.savefig(agent_folder + '/learning_plot.png')

# Describe mean values over number of runs
for run in range(len(win_rates)):
    log.add_table_line('Average over runs', [run+1, np.mean(win_rates[:run+1])])

# Process time values
process_time = (timeit.default_timer() - start_t)/60
av_run_time = process_time / n_runs
log.add_line("Total Wall time (mins) = " + str(process_time))
log.add_line("Average Wall time per run (mins) = " + str(av_run_time))

# Report and save log data
log.report()
log.dump()
with open(folder + 'log.data', 'wb') as handle:
    pickle.dump(log, handle)