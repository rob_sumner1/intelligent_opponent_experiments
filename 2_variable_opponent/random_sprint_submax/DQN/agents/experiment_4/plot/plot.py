import os
import pickle
import numpy as np
import matplotlib.pyplot as plt

# Load data from saved table pickle files
spaces = [3, 5, 7, 9, 11, 13, 15, 17, 19]
log_files = []
avg_rate_table = []
sep_tables = []
n_run_average_table = []
for space in spaces:
    log = pickle.load( open("space_" + str(space) + "_log.data", 'rb') )
    log_files.append(log)
    avg_rate_table.append(log.tables['Win Rate Comparison table'])
    sep_tables.append(log.tables['Average Separation Comparison table'])
    n_run_average_table.append(log.tables['Averaged Win Rate Comparison'])

# Stack results as numpy array.
n_run_average_data = np.ndarray((7, 5, len(spaces))) # Stores win rates averaged across n runs.
run_data = np.ndarray((7, 5, len(spaces))) # Stores win rates for each run
for i in range(len(spaces)):
    n_run_average_data[:,:,i] = np.array(n_run_average_table[i].table_lines)[:, 1:]
    run_data[:,:,i] = np.array(avg_rate_table[i].table_lines)[:, 1:]

# Use Latex intepreter.
from matplotlib import rc
import matplotlib.font_manager
rc('font', **{'family':'CMU Serif'})
rc('text', usetex=True)

# Plot for only 7 run average with variance
plt.plot(spaces, 100*n_run_average_data[6,4,:], label=r"Mean $\mu$")
error = np.sqrt(np.var(100*run_data[:,4,:], axis=0))
plt.fill_between(spaces,
                 100*n_run_average_data[6,4,:] - error,
                 100*n_run_average_data[6,4,:] + error,
                 facecolor='red',  alpha=0.3, interpolate=True, label=r"$\mu \pm \sigma$")
plt.xlabel("$|Action Space|$")
plt.ylabel("Average Win Percentage After Training")
plt.title(r"Win Percentage against Size of Action Space (Average of 7 $\times$ 150000 steps)")
plt.legend()
plt.ylim([0,100])
plt.xlim([3,19])
plt.tight_layout()
plt.savefig('run_av_7.png', dpi=480)
plt.show()

# Plot for only 7 run average with max & min
plt.plot(spaces, 100*n_run_average_data[6,4,:], label=r"Mean Performance")
min_val = np.min(100*run_data[:,4,:], axis=0)
max_val = np.max(100*run_data[:,4,:], axis=0)
plt.fill_between(spaces, min_val, max_val, facecolor='red',  alpha=0.3,
                 interpolate=True, label=r"Performance Range")
plt.xlabel("$|Action Space|$")
plt.ylabel("Average Win Percentage After Training")
plt.title(r"Win Percentage against Size of Action Space (Average of 7 $\times$ 150000 steps)")
plt.legend()
plt.ylim([0,100])
plt.xlim([3,19])
plt.tight_layout()
plt.savefig('run_av_range_7.png', dpi=480)
plt.show()

# Plot for n-run averages
for i in range(5):
    plt.plot(spaces, 100*n_run_average_data[2+i,4,:], label=str(3+i) +" Run average")
plt.xlabel("$|Action Space|$")
plt.ylabel("Average Win Percentage After Training")
plt.title(r"Win Percentage against Size of Action Space (Average of n $\times$ 150000 steps)")
plt.legend()
plt.ylim([0,100])
plt.xlim([2,19])
plt.tight_layout()
plt.savefig('run_vary_av.png', dpi=480)
plt.show()

# Plot for varying step
for i in range(5):
    plt.plot(spaces, 100*n_run_average_data[6,i,:], label=str((i+2)*25000)+" Training Steps")
plt.xlabel("$|Action Space|$")
plt.ylabel("Average Win Percentage After Training")
plt.title(r"Win Percentage against Size of Action Space (Average of 7 $\times$ m steps)")
plt.legend()
plt.ylim([0,100])
plt.xlim([2,19])
plt.tight_layout()
plt.savefig('step_vary_av.png', dpi=480)
plt.show()

# Plot for the proposed solution - variance
# Best estimator - 7 runs of 150,000
plt.plot(spaces, 100*n_run_average_data[6,4,:], 'red', label="7 runs of 150000 steps")
error1 = np.sqrt(np.var(100*run_data[:,4,:], axis=0))
plt.fill_between(spaces,
                 100*n_run_average_data[6,4,:] - error,
                 100*n_run_average_data[6,4,:] + error,
                 facecolor='red',  alpha=0.3, interpolate=True)
# 4 runs of 150,000 steps
plt.plot(spaces, 100*n_run_average_data[4,3,:], 'blue', label="4 runs of 150000 steps")
error2 = np.sqrt(np.var(100*run_data[:5,3,:], axis=0))
plt.fill_between(spaces,
                 100*n_run_average_data[4,2,:] - error2,
                 100*n_run_average_data[4,2,:] + error2,
                 facecolor='blue',  alpha=0.3, interpolate=True)
plt.xlabel("$|Action Space|$")
plt.ylabel("Average Win Percentage After Training")
plt.title("Comparison of Estimators on Action Space Problem")
plt.legend()
plt.ylim([0,100])
plt.xlim([3,19])
plt.tight_layout()
plt.savefig('comparison_var.png', dpi=480)
plt.show()

# Plot for the proposed solution - max/min
# Best estimator - 7 runs of 150,000
plt.plot(spaces, 100*n_run_average_data[6,4,:], 'red', label="7 runs of 150000 steps")
min_val1 = np.min(100*run_data[:,4,:], axis=0)
max_val1 = np.max(100*run_data[:,4,:], axis=0)
plt.fill_between(spaces, min_val1, max_val1,
                 facecolor='red',  alpha=0.3, interpolate=True)
# 4 runs of 150,000 steps
plt.plot(spaces, 100*n_run_average_data[4,3,:], 'blue', label="4 runs of 150000 steps")
min_val2 = np.min(100*run_data[:5,3,:], axis=0)
max_val2 = np.max(100*run_data[:5,3,:], axis=0)
plt.fill_between(spaces, min_val2, max_val2,
                 facecolor='blue',  alpha=0.3, interpolate=True)

plt.xlabel("$|Action Space|$")
plt.ylabel("Average Win Percentage After Training")
plt.title("Comparison of Estimators on Action Space Problem")
plt.legend()
plt.ylim([0,100])
plt.xlim([3,19])
plt.tight_layout()
plt.savefig('comparison_maxmin.png', dpi=480)
plt.show()

# Calculate difference between strings.
def get_ranking(val_array):
    temp = val_array.argsort()
    ranks = np.empty_like(temp)
    ranks[temp] = np.arange(len(val_array))
    return ranks

# Damerau levenshtein distance.
def damerau_levenshtein_distance(s1, s2):
    d = {}
    lenstr1 = len(s1)
    lenstr2 = len(s2)
    for i in range(-1,lenstr1+1):
        d[(i,-1)] = i+1
    for j in range(-1,lenstr2+1):
        d[(-1,j)] = j+1

    for i in range(lenstr1):
        for j in range(lenstr2):
            if s1[i] == s2[j]:
                cost = 0
            else:
                cost = 1
            d[(i,j)] = min(
                           d[(i-1,j)] + 1, # deletion
                           d[(i,j-1)] + 1, # insertion
                           d[(i-1,j-1)] + cost, # substitution
                          )
            if i and j and s1[i]==s2[j-1] and s1[i-1] == s2[j]:
                d[(i,j)] = min (d[(i,j)], d[i-2,j-2] + cost) # transposition

    return d[lenstr1-1,lenstr2-1]

# Ideal ranking for 7 x 150,000
best_ranking = get_ranking(n_run_average_data[6,4,:])
rank_distance = np.zeros((5,5))
dl_distance = np.zeros((5,5))

# Compare ranking
# rank distance = (3+i run average, 2+j*25000 steps)
for i in range(5):
    for j in range(5):
        rank_2 = get_ranking(n_run_average_data[2+i,j,:])
        rank_distance[i,j] = np.sum((rank_2 - best_ranking)**2)
        dl_distance[i,j] = damerau_levenshtein_distance(best_ranking, rank_2)
print(rank_distance)
print(dl_distance)







