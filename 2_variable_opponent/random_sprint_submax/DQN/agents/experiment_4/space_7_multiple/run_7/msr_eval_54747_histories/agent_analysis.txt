LEARNING LOG 

Time: 12/02/2020 17:05:49
Evaluation of model defined in msr_eval_54747

Sprint Distance Table:
 |Sprint Distance  |Reward           |Final Sep.       |Win?             |
 |650.0            |5.09206445558095 |4.09206445558095 |Y                |
 |640.0            |4.87856035053619 |3.87856035053619 |Y                |
 |630.0            |4.47687093222521 |3.47687093222521 |Y                |
 |620.0            |4.15326452534770 |3.15326452534770 |Y                |
 |610.0            |3.58140358517312 |2.58140358517312 |Y                |
 |600.0            |3.14088178500935 |2.14088178500935 |Y                |
 |590.0            |2.65159787140987 |1.65159787140987 |Y                |
 |580.0            |2.11248404886475 |1.11248404886475 |Y                |
 |570.0            |1.21176448061839 |0.21176448061839 |Y                |
 |560.0            |0.75243603771811 |-0.4297385710412 |N                |
 |550.0            |0.75391719249300 |-1.0675782365010 |N                |
 |540.0            |0.74915582064821 |-1.9934253935546 |N                |
 |530.0            |3.61308489661291 |2.61308489661291 |Y                |
 |520.0            |5.57921498133248 |4.57921498133248 |Y                |
 |510.0            |5.2642298611554  |4.2642298611554  |Y                |
 |500.0            |6.11584799940976 |5.11584799940976 |Y                |
 |490.0            |5.88672184914298 |4.88672184914298 |Y                |
 |480.0            |6.17025980321329 |5.17025980321329 |Y                |
 |470.0            |5.99169465309216 |4.99169465309216 |Y                |
 |460.0            |5.76503623783878 |4.76503623783878 |Y                |
 |450.0            |5.85391945758624 |4.85391945758624 |Y                |
 |440.0            |5.65810130617796 |4.65810130617796 |Y                |
 |430.0            |5.41266441532025 |4.41266441532025 |Y                |
 |420.0            |5.11634371180889 |4.11634371180889 |Y                |
 |410.0            |4.76782334737072 |3.76782334737072 |Y                |
 |400.0            |4.36575392539521 |3.36575392539521 |Y                |
 |390.0            |3.90877086969601 |2.90877086969601 |Y                |
 |380.0            |3.39551295710396 |2.39551295710396 |Y                |
 |370.0            |2.82464460765515 |1.82464460765515 |Y                |
 |360.0            |2.19494573024201 |1.19494573024201 |Y                |
 |350.0            |2.66709052466069 |1.66709052466069 |Y                |
 |340.0            |2.05100270977447 |1.05100270977447 |Y                |
 |330.0            |1.37707384227292 |0.37707384227292 |Y                |
 |320.0            |0.75759020790112 |-0.3352351436614 |N                |
 |310.0            |0.75659071438057 |-1.0359070726759 |N                |
 |300.0            |0.75546890609458 |-1.7010771078255 |N                |
 |290.0            |0.75422250145107 |-2.3269656037181 |N                |
 |280.0            |0.75284643855434 |-2.9121083679992 |N                |
 |270.0            |0.75133571067075 |-3.4552081983564 |N                |
 |260.0            |0.74968577629251 |-3.9549945935746 |N                |
 |250.0            |0.74789288358960 |-4.4102164189912 |N                |

Win rate = 0.7317073170731707
Average separation value = 1.747711585700691
