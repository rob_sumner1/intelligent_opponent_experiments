import numpy as np
import matplotlib.pyplot as plt

result_files = ["results_0_1.txt", "results_0_2.txt", \
                "results_0_3.txt", "results_0_4.txt", \
                "results_0_5.txt",]
# result_files = ["results_0_1.txt"]

start_line = 44
end_line = 83

results = []
for f in result_files:
    data = open(f, 'r').readlines()
    array = np.ndarray((end_line-start_line+1, 2))
    j = 0
    for i in range(start_line-1, end_line):
        l = data[i].split(' - ')
        # Remove any end of line tokens.
        array[j, 0] = float(l[0].replace('\n',''))
        array[j, 1] = float(l[1].replace('\n',''))
        j += 1
    results.append(array)

# Set interpreter
from matplotlib import rc
rc('font', **{'family':'serif','serif':['Computer Modern Roman']})
rc('text', usetex=True)

# Plot results
labels = ['0.1 (s)', '0.2 (s)', '0.3 (s)', '0.4 (s)', '0.5 (s)']
for i in range(len(results)):
    data = results[i]
    plt.plot(data[:,0], data[:,1], label=labels[i])
plt.xlabel('Sprint Distance, m')
plt.ylabel('Winning margin of Rider A, m')
plt.title('Winning margin of Rider A for varying timestep')
plt.xlim([250,650])
plt.legend()
plt.tight_layout()
plt.savefig('results.png', dpi=400)
plt.show()




