import numpy as np
import matplotlib.pyplot as plt

# Read in experiment data
start_line = 44
end_line = 83
data = open("results.txt", 'r').readlines()
array_1 = np.ndarray((end_line-start_line+1, 2))
j = 0
for i in range(start_line-1, end_line):
    l = data[i].split(' - ')
    # Remove any end of line tokens.
    array_1[j, 0] = float(l[0].replace('\n',''))
    array_1[j, 1] = float(l[1].replace('\n',''))
    j += 1

# Read in Mark's data
data = open("mark_results.txt", 'r').readlines()
array_2 = np.ndarray((len(data), 2))
j = 0
for i in range(len(data)):
    l = data[i].split(' - ')
    # Remove any end of line tokens.
    array_2[j, 0] = float(l[0].replace('\n',''))
    array_2[j, 1] = float(l[1].replace('\n',''))
    j += 1

print(array_1)
# Set interpreter
from matplotlib import rc
rc('font', **{'family':'serif','serif':['Computer Modern Roman']})
rc('text', usetex=True)

# Plot results
plt.plot(array_1[:,0], array_1[:,1], label="RL Results")
plt.plot(array_2[:,0], array_2[:,1], label="Hollands' Results")
plt.xlabel('Sprint Distance, m')
plt.ylabel('Winning margin of Rider A, m')
plt.xlim([250,650])
plt.legend()
plt.tight_layout()
plt.savefig('results.png', dpi=400)
plt.show()




