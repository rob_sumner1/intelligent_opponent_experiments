import numpy as np
import matplotlib.pyplot as plt

result_files = ["expanded_results_11.txt",
                "expanded_results_13.txt",
                "expanded_results_15.txt",
                "expanded_results_17.txt"]

start_line = 7
end_line = 46

results = []
for f in result_files:
    data = open(f, 'r').readlines()
    array = np.ndarray((end_line-start_line+1, 2))
    j = 0
    for i in range(start_line-1, end_line):
        l = data[i].split('|')
        # Remove any end of line tokens.
        array[j, 0] = float(l[1].replace('\n',''))
        array[j, 1] = float(l[3].replace('\n',''))
        j += 1
    results.append(array)

# Load Mark's results
data = open("mark_results.txt", 'r').readlines()
m_array = np.ndarray((len(data)-1,2))
for i in range(len(data)-1):
    l = data[i].split(' - ')
    m_array[i,0] = float(l[0].replace('\n',''))
    m_array[i,1] = float(l[1].replace('\n',''))

# Set interpreter
from matplotlib import rc
rc('font', **{'family':'serif','serif':['Computer Modern Roman']})
rc('text', usetex=True)

# Plot results
labels = ['$|A|=11$', '$|A|=13$', '$|A|=15$', '$|A|=17$']
for i in range(len(results)):
    data = results[i]
    plt.plot(data[:,0], data[:,1], label=labels[i])
plt.plot(m_array[:,0], m_array[:,1], label="Hollands' Results")
plt.xlabel('Sprint Distance, m')
plt.ylabel('Winning Margin of Rider A, m')
plt.title('Winning Margin of Rider A for Varying Action Space Size')
plt.xlim([250,650])
plt.legend()
plt.tight_layout()
plt.savefig('results.png', dpi=400)
plt.show()




