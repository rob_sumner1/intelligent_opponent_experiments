from learning_tools import plot_action_separation, plot_fatigue, plot_velocities

hist = ["hist_100.hist", "hist_200.hist", "hist_300.hist", "hist_400.hist", "hist_500.hist"]
titles = ["Sprint Distance: 650 m", "Sprint Distance: 550 m", "Sprint Distance: 450 m", "Sprint Distance: 350 m", 
"Sprint Distance: 250 m"]

suptitle1 = "Action-Separation Graphs for Varying Sprint Distance with $|A_{t}| = 15$" 
plot_action_separation(histories=hist, titles=titles, suptitle=suptitle1, save=True)

suptitle2 = "Rider Fatigue Graphs for Varying Sprint Distance with $|A_{t}| = 15$"
plot_fatigue(histories=hist, titles=titles, suptitle=suptitle2, save=True)

suptitle3 = "Rider Velocity Graphs for Varying Sprint Distance with $|A_{t}| = 15$"
plot_velocities(histories=hist, titles=titles, suptitle=suptitle3, save=True)
