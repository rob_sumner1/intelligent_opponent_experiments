from learning_tools import plot_action_separation, plot_fatigue

hist = ["hist_100.hist", "hist_200.hist", "hist_300.hist", "hist_400.hist", "hist_500.hist"]
titles = ["Sprint Distance: 650 m", "Sprint Distance: 550 m", "Sprint Distance: 450 m", "Sprint Distance: 350 m", "Sprint Distance: 250 m"]
suptitle1 = "Action-Separation Graphs for Varying Sprint Distance with $|A_{t}| = 9$"
suptitle2 = "Rider Fatigue Graphs for Varying Sprint Distance with $|A_{t}| = 9$" 
plot_action_separation(histories=hist, titles=titles, suptitle=suptitle1, save=True)
plot_fatigue(histories=hist, titles=titles, suptitle=suptitle2, save=True)
