# Results from investigation into action space size. 
import matplotlib.pyplot as plt
import numpy as np

action_space = np.array([2,3,5,7,9,11,13,15,17,19])
vals = np.array([5.8, 6.2, 7.0, 7.2, 7.6, 7.6, 8.6, 8.2, 7.8, 6.8])

from matplotlib import rc
rc('font', **{'family':'serif','serif':['Computer Modern Roman']})
rc('text', usetex=True)

plt.plot(action_space, 100*vals/9)
plt.xlabel("$|Action Space|$")
plt.ylabel("Average Win Percentage After Training")
plt.title("Win Percentage against Size of Action Space")
plt.ylim([0,100])
plt.xlim([2,19])
plt.tight_layout()
plt.savefig('results.png', dpi=480)
plt.show()

