LEARNING LOG 

Time: 02/02/2020 17:26:33
Evaluation of model defined in msr_eval_53349

Sprint Distance Table:
 |Sprint Distance  |Reward           |Final Sep.       |Win?             |
 |650.0            |3.89251570675457 |2.89251570675457 |Y                |
 |640.0            |3.91444845802789 |2.91444845802789 |Y                |
 |630.0            |4.10527006277016 |3.10527006277015 |Y                |
 |620.0            |4.45406440811711 |3.45406440811711 |Y                |
 |610.0            |4.42664625259271 |3.42664625259271 |Y                |
 |600.0            |4.90669546399806 |3.90669546399806 |Y                |
 |590.0            |5.37458877320250 |4.37458877320250 |Y                |
 |580.0            |5.52540027667623 |4.52540027667623 |Y                |
 |570.0            |5.56474605099629 |4.56474605099629 |Y                |
 |560.0            |4.21387283882654 |3.21387283882654 |Y                |
 |550.0            |2.04619721181188 |1.04619721181188 |Y                |
 |540.0            |0.74112458771588 |-8.2003086601821 |N                |
 |530.0            |0.73117625561980 |-16.439450924330 |N                |
 |520.0            |0.72226416766567 |-19.558574033144 |N                |
 |510.0            |0.72484230543359 |-18.872305261936 |N                |
 |500.0            |0.72744996102984 |-18.182383142712 |N                |
 |490.0            |0.72325669809851 |-17.596818993197 |N                |
 |480.0            |0.72130002323405 |-19.703981478399 |N                |
 |470.0            |0.72377633005863 |-18.963693685403 |N                |
 |460.0            |0.72629108751695 |-18.209960309086 |N                |
 |450.0            |0.72660779273584 |-14.341065815118 |N                |
 |440.0            |0.72911435301162 |-13.553846206406 |N                |
 |430.0            |0.73277847630492 |-12.111561319953 |N                |
 |420.0            |0.73683054721575 |-10.490896897205 |N                |
 |410.0            |0.73999374810091 |-9.4048920382946 |N                |
 |400.0            |0.74085216789914 |-4.9797590685986 |N                |
 |390.0            |0.74393085105019 |-3.9240000015770 |N                |
 |380.0            |0.74718822448874 |-2.7985889226483 |N                |
 |370.0            |0.75047801178074 |-1.6824777071697 |N                |
 |360.0            |1.68398750868152 |0.68398750868152 |Y                |
 |350.0            |2.96189613825276 |1.96189613825276 |Y                |
 |340.0            |2.53703019683257 |1.53703019683257 |Y                |
 |330.0            |3.04831597094471 |2.04831597094471 |Y                |
 |320.0            |3.68779666439741 |2.68779666439741 |Y                |
 |310.0            |3.93841073166652 |2.93841073166652 |Y                |
 |300.0            |3.07963423682576 |2.07963423682576 |Y                |
 |290.0            |1.08566897886021 |0.08566897886021 |Y                |
 |280.0            |0.76179257273535 |-0.3374050088990 |N                |
 |270.0            |0.76069249118757 |-0.4373419613284 |N                |
 |260.0            |0.75945862227808 |-0.5019970226685 |N                |
 |250.0            |0.75809171297319 |-0.5273597422570 |N                |

Win rate = 0.4634146341463415
Average separation value = -4.374914201714191
LEARNING LOG 

Time: 02/02/2020 19:02:51
Evaluation of model defined in msr_eval_53349

Sprint Distance Table:
 |Sprint Distance  |Reward           |Final Sep.       |Win?             |
 |650.0            |3.89251570675457 |2.89251570675457 |Y                |
 |640.0            |3.91444845802789 |2.91444845802789 |Y                |
 |630.0            |4.10527006277016 |3.10527006277015 |Y                |
 |620.0            |4.45406440811711 |3.45406440811711 |Y                |
 |610.0            |4.42664625259271 |3.42664625259271 |Y                |
 |600.0            |4.90669546399806 |3.90669546399806 |Y                |
 |590.0            |5.37458877320250 |4.37458877320250 |Y                |
 |580.0            |5.52540027667623 |4.52540027667623 |Y                |
 |570.0            |5.56474605099629 |4.56474605099629 |Y                |
 |560.0            |4.21387283882654 |3.21387283882654 |Y                |
 |550.0            |2.04619721181188 |1.04619721181188 |Y                |
 |540.0            |0.74112458771588 |-8.2003086601821 |N                |
 |530.0            |0.73117625561980 |-16.439450924330 |N                |
 |520.0            |0.72226416766567 |-19.558574033144 |N                |
 |510.0            |0.72484230543359 |-18.872305261936 |N                |
 |500.0            |0.72744996102984 |-18.182383142712 |N                |
 |490.0            |0.72325669809851 |-17.596818993197 |N                |
 |480.0            |0.72130002323405 |-19.703981478399 |N                |
 |470.0            |0.72377633005863 |-18.963693685403 |N                |
 |460.0            |0.72629108751695 |-18.209960309086 |N                |
 |450.0            |0.72660779273584 |-14.341065815118 |N                |
 |440.0            |0.72911435301162 |-13.553846206406 |N                |
 |430.0            |0.73277847630492 |-12.111561319953 |N                |
 |420.0            |0.73683054721575 |-10.490896897205 |N                |
 |410.0            |0.73999374810091 |-9.4048920382946 |N                |
 |400.0            |0.74085216789914 |-4.9797590685986 |N                |
 |390.0            |0.74393085105019 |-3.9240000015770 |N                |
 |380.0            |0.74718822448874 |-2.7985889226483 |N                |
 |370.0            |0.75047801178074 |-1.6824777071697 |N                |
 |360.0            |1.68398750868152 |0.68398750868152 |Y                |
 |350.0            |2.96189613825276 |1.96189613825276 |Y                |
 |340.0            |2.53703019683257 |1.53703019683257 |Y                |
 |330.0            |3.04831597094471 |2.04831597094471 |Y                |
 |320.0            |3.68779666439741 |2.68779666439741 |Y                |
 |310.0            |3.93841073166652 |2.93841073166652 |Y                |
 |300.0            |3.07963423682576 |2.07963423682576 |Y                |
 |290.0            |1.08566897886021 |0.08566897886021 |Y                |
 |280.0            |0.76179257273535 |-0.3374050088990 |N                |
 |270.0            |0.76069249118757 |-0.4373419613284 |N                |
 |260.0            |0.75945862227808 |-0.5019970226685 |N                |
 |250.0            |0.75809171297319 |-0.5273597422570 |N                |

Win rate = 0.4634146341463415
Average separation value = -4.374914201714191
LEARNING LOG 

Time: 03/02/2020 14:11:56
Evaluation of model defined in msr_eval_53349

Sprint Distance Table:
 |Sprint Distance  |Reward           |Final Sep.       |Win?             |
 |650.0            |3.89251570675457 |2.89251570675457 |Y                |
 |640.0            |3.91444845802789 |2.91444845802789 |Y                |
 |630.0            |4.10527006277016 |3.10527006277015 |Y                |
 |620.0            |4.45406440811711 |3.45406440811711 |Y                |
 |610.0            |4.42664625259271 |3.42664625259271 |Y                |
 |600.0            |4.90669546399806 |3.90669546399806 |Y                |
 |590.0            |5.37458877320250 |4.37458877320250 |Y                |
 |580.0            |5.52540027667623 |4.52540027667623 |Y                |
 |570.0            |5.56474605099629 |4.56474605099629 |Y                |
 |560.0            |4.21387283882654 |3.21387283882654 |Y                |
 |550.0            |2.04619721181188 |1.04619721181188 |Y                |
 |540.0            |0.74112458771588 |-8.2003086601821 |N                |
 |530.0            |0.73117625561980 |-16.439450924330 |N                |
 |520.0            |0.72226416766567 |-19.558574033144 |N                |
 |510.0            |0.72484230543359 |-18.872305261936 |N                |
 |500.0            |0.72744996102984 |-18.182383142712 |N                |
 |490.0            |0.72325669809851 |-17.596818993197 |N                |
 |480.0            |0.72130002323405 |-19.703981478399 |N                |
 |470.0            |0.72377633005863 |-18.963693685403 |N                |
 |460.0            |0.72629108751695 |-18.209960309086 |N                |
 |450.0            |0.72660779273584 |-14.341065815118 |N                |
 |440.0            |0.72911435301162 |-13.553846206406 |N                |
 |430.0            |0.73277847630492 |-12.111561319953 |N                |
 |420.0            |0.73683054721575 |-10.490896897205 |N                |
 |410.0            |0.73999374810091 |-9.4048920382946 |N                |
 |400.0            |0.74085216789914 |-4.9797590685986 |N                |
 |390.0            |0.74393085105019 |-3.9240000015770 |N                |
 |380.0            |0.74718822448874 |-2.7985889226483 |N                |
 |370.0            |0.75047801178074 |-1.6824777071697 |N                |
 |360.0            |1.68398750868152 |0.68398750868152 |Y                |
 |350.0            |2.96189613825276 |1.96189613825276 |Y                |
 |340.0            |2.53703019683257 |1.53703019683257 |Y                |
 |330.0            |3.04831597094471 |2.04831597094471 |Y                |
 |320.0            |3.68779666439741 |2.68779666439741 |Y                |
 |310.0            |3.93841073166652 |2.93841073166652 |Y                |
 |300.0            |3.07963423682576 |2.07963423682576 |Y                |
 |290.0            |1.08566897886021 |0.08566897886021 |Y                |
 |280.0            |0.76179257273535 |-0.3374050088990 |N                |
 |270.0            |0.76069249118757 |-0.4373419613284 |N                |
 |260.0            |0.75945862227808 |-0.5019970226685 |N                |
 |250.0            |0.75809171297319 |-0.5273597422570 |N                |

Win rate = 0.4634146341463415
Average separation value = -4.374914201714191
