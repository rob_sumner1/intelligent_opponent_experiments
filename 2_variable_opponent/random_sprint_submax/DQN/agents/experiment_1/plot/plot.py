import os
import pickle
import numpy as np
import matplotlib.pyplot as plt

# Load data from saved table pickle files
spaces = [3, 5, 7, 9, 11, 13, 15, 17, 19]
log_files = []
avg_rate_table = []
sep_tables = []
n_run_average_table = []
for space in spaces:
    log = pickle.load( open("space_" + str(space) + "_log.data", 'rb') )
    log_files.append(log)
    avg_rate_table.append(log.tables['Win Rate Comparison table'])
    sep_tables.append(log.tables['Average Separation Comparison table'])
    n_run_average_table.append(log.tables['Averaged Win Rate Comparison'])

# Stack results as numpy array.
n_run_average_data = np.ndarray((7, 5, len(spaces))) # Stores win rates averaged across n runs.
run_data = np.ndarray((7, 5, len(spaces))) # Stores win rates for each run
for i in range(len(spaces)):
    n_run_average_data[:,:,i] = np.array(n_run_average_table[i].table_lines)[:, 1:]
    run_data[:,:,i] = np.array(avg_rate_table[i].table_lines)[:, 1:]

# Use Latex intepreter.
from matplotlib import rc
import matplotlib.font_manager
rc('font', **{'family':'CMU Serif'})
rc('text', usetex=True)

# Plot for only 7 run average
plt.plot(spaces, 100*n_run_average_data[6,4,:], label=r"Mean $\mu$")
error = np.sqrt(np.var(100*run_data[:,4,:], axis=0))
plt.fill_between(spaces,
                 100*n_run_average_data[6,4,:] - error,
                 100*n_run_average_data[6,4,:] + error,
                 facecolor='red',  alpha=0.3, interpolate=True, label=r"$\mu \pm \sigma$")
plt.xlabel("$|Action Space|$")
plt.ylabel("Average Win Percentage After Training")
plt.title(r"Win Percentage against Size of Action Space (Average of 7 $\times$ 150000 steps)")
plt.legend()
plt.ylim([0,100])
plt.xlim([3,19])
plt.tight_layout()
plt.savefig('run_av_7.png', dpi=480)
plt.show()

# Plot for n-run averages
for i in range(5):
    plt.plot(spaces, 100*n_run_average_data[2+i,4,:], label=str(3+i) +" Run average")
plt.xlabel("$|Action Space|$")
plt.ylabel("Average Win Percentage After Training")
plt.title(r"Win Percentage against Size of Action Space (Average of n $\times$ 150000 steps)")
plt.legend()
plt.ylim([0,100])
plt.xlim([2,19])
plt.tight_layout()
plt.savefig('run_vary_av.png', dpi=480)
plt.show()

# Plot for varying step
for i in range(5):
    plt.plot(spaces, 100*n_run_average_data[6,i,:], label=str((i+2)*25000)+" Training Steps")
plt.xlabel("$|Action Space|$")
plt.ylabel("Average Win Percentage After Training")
plt.title(r"Win Percentage against Size of Action Space (Average of 7 $\times$ m steps)")
plt.legend()
plt.ylim([0,100])
plt.xlim([2,19])
plt.tight_layout()
plt.savefig('step_vary_av.png', dpi=480)
plt.show()

# Plot for the proposed solution.
# Best estimator - 7 runs of 150,000
plt.plot(spaces, 100*n_run_average_data[6,4,:], 'red', label="7 runs of 150000 steps")
error1 = np.sqrt(np.var(100*run_data[:,4,:], axis=0))
plt.fill_between(spaces,
                 100*n_run_average_data[6,4,:] - error,
                 100*n_run_average_data[6,4,:] + error,
                 facecolor='red',  alpha=0.3, interpolate=True)
# 4 runs of 100,000 steps
plt.plot(spaces, 100*n_run_average_data[4,2,:], 'blue', label="5 runs of 100000 steps")
error2 = np.sqrt(np.var(100*run_data[:5,2,:], axis=0))
print(error2)
plt.fill_between(spaces,
                 100*n_run_average_data[4,2,:] - error2,
                 100*n_run_average_data[4,2,:] + error2,
                 facecolor='blue',  alpha=0.3, interpolate=True)

plt.xlabel("$|Action Space|$")
plt.ylabel("Average Win Percentage After Training")
plt.title("Comparison of Estimators on Action Space Problem")
plt.legend()
plt.ylim([0,100])
plt.xlim([3,19])
plt.tight_layout()
plt.savefig('comparison.png', dpi=480)
plt.show()








