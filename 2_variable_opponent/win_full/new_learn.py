import os
import gym
import gym_msr
import numpy as np
from stable_baselines import DQN, results_plotter
from stable_baselines.bench import Monitor
from stable_baselines.deepq.policies import MlpPolicy
from stable_baselines.results_plotter import load_results, ts2xy
from learning_tools import get_last_agent
import matplotlib.pyplot as plt

# Define call back function
best_mean_reward, n_steps = -np.inf, 0

def callback(_locals, _globals):
    """
    Callback called at each step (for DQN an others) or after n steps (see ACER or PPO2)
    :param _locals: (dict)
    :param _globals: (dict)
    """
    global n_steps, best_mean_reward
    # Print stats every 1000 calls
    if (n_steps + 1) % 1000 == 0:
        # Evaluate policy training performance
        x, y = ts2xy(load_results(log_dir), 'timesteps')
        if len(x) > 0:
            mean_reward = np.mean(y[-100:])
            print(x[-1], 'timesteps')
            print("Best mean reward: {:.2f} - Last mean reward per episode: {:.2f}".format(best_mean_reward, mean_reward))

            # New best model, you could save the agent here
            if mean_reward > best_mean_reward:
                best_mean_reward = mean_reward
                # Example for saving best model
                print("Saving new best model")
                _locals['self'].save(agent_folder + '/msr_callback_' + str(n_steps))

    n_steps += 1
    return True

# Places to save data
folder = os.path.dirname(os.path.abspath(__file__)) + '/models/DQN/agents/experiment/'
time_steps = 100000
best_rewards = []

for i in range(5):
    # Reset values
    best_mean_reward, n_steps = -np.inf, 0

    # Setup experiment and log folder and save directory
    agent_folder = folder + "run_" + str(i+1)
    os.makedirs(agent_folder, exist_ok=True)
    log_dir = agent_folder + '/log'
    os.makedirs(log_dir, exist_ok=True)

    # Create environment with a monitor
    env = gym.make('msr-ident-win-v2')
    env = Monitor(env, log_dir, allow_early_resets=True)

    # Create and train agent.
    model = DQN(MlpPolicy, env, verbose=0)
    model.learn(total_timesteps=time_steps, callback=callback)
    best_rewards.append(best_mean_reward)

    # Visualise training results
    results_plotter.plot_results([log_dir], time_steps, results_plotter.X_TIMESTEPS, "750m Velo")
    plt.savefig(agent_folder + '/learning_plot.png')

print("Best rewards achieved: ", best_rewards)
print("Average best reward achieved: ", np.mean(best_rewards))
print("Aveagre reward variance: ", np.var(best_rewards))
best_rewards.append(np.mean(best_rewards))
np.savetxt(folder + "best_rewards", best_rewards)


