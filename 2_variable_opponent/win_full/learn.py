import os
import gym
import gym_msr
from stable_baselines import DQN
from stable_baselines.deepq.policies import MlpPolicy
from learning_tools import get_last_agent, LearningLog

# Parameters
epoch_timesteps = 10000

# Learning environment to use.
env = gym.make('msr-ident-win-v2')

# Learning location & logs
folder = os.path.dirname(os.path.abspath(__file__)) + '/models/DQN/'
tensor_log = "./models/DQN/log/"

#  Set up log file
log = LearningLog()
log.set_description("Set of learning runs, each corresponding to " +
			 str(epoch_timesteps) + " timesteps.")
log.set_table(['Iteration', 'Final Reward', 'Average Action'])

# Learn with deepQ policy
for i in range(10):
    # Load most recent file
    agent_path, save_path = get_last_agent(folder + "agents")
    if agent_path is None:
        print('New model...')
        model = DQN(MlpPolicy, env, verbose=1, tensorboard_log=tensor_log)
    else:
        print('Loading model: ' + agent_path)
        model = DQN.load(agent_path, tensorboard_log=tensor_log)
        model.set_env(env)

    # Train & save model for timesteps.
    model.learn(total_timesteps=epoch_timesteps)
    model.save(save_path)

    # Track whether current agent wins.
    obs = env.reset()
    act = 0
    its = 0
    while not env.complete():
        action, _states = model.predict(obs)
        act += action
        its += 1
        obs, rewards, dones, info = env.step(action)

    # Save history
    name = "run_" + str(i+1)
    hist_folder = folder + "histories"
    env.save_history(folder=hist_folder, name=name)
    env.reset()

    # Store log data
    log.add_table_line([i+1, rewards, act/its])

# Write out the log data.
log.dump()
