import os
import gym
import gym_msr
from stable_baselines import DQN
from stable_baselines.deepq.policies import MlpPolicy
from learning_tools import get_agents, save_history

# Get all agents in folder.
current_folder = os.path.dirname(os.path.abspath(__file__))
agent_paths = get_agents(current_folder, 'msr_')

# # Load a specific agent
# current_folder = os.path.dirname(os.path.abspath(__file__))
# agent_paths = [os.path.dirname(current_folder) + "/agents/msr_agent_1"]

for agent_path in agent_paths:
    # Load correct model.
    env = gym.make('msr-ident-win-v2')
    model = DQN.load(agent_path)
    model.set_env(env)

    # Save name
    save_name = agent_path.split('/')[-1]

    # Save history
    save_history(env, model, save_name=save_name, view=False)

