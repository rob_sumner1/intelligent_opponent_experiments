#!/bin/bash
echo "Finding results..." 
pattern="run_"
for d in *"${pattern}"* ; do
    # Change into run_ directory
    echo "$d"
    cd "$d"
    
    # Run get histories
    cp ../get_histories.py .
    python get_histories.py 2> ../shell_output.txt
    
    # Clean up 
    rm get_histories.py
    mkdir plots
    mv *.hist plots/.
    mv reward_data.txt plots/.
    cd ..
done


