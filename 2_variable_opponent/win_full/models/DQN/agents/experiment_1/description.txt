Experiment 1: Using 15 runs of 25000 steps with DQN algorithm agent. 

The environment is the 750m velo sprint environment: 
Action space - Discrete, 2 options (0, 1)
Observation space - Unnormalised, low = [0, 0, 0, 0, 0, 0] and high = [800, 50, 1, 800, 50, 1]
time step = 0.3
Reward  - 0.75*(elapsed distance/750) if the rider loses.
	- 1 + separation distance if the rider wins. 
	- 0 if the race is incomplete. 
