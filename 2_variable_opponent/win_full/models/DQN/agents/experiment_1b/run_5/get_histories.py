import os
import gym
import gym_msr
import numpy as np
from stable_baselines import DQN
from stable_baselines.deepq.policies import MlpPolicy
from learning_tools import get_agents, save_history

# Get all agents in folder.
current_folder = os.path.dirname(os.path.abspath(__file__))
agent_paths = get_agents(current_folder, 'msr_')

# # Load a specific agent
# current_folder = os.path.dirname(os.path.abspath(__file__))
# agent_paths = [os.path.dirname(current_folder) + "/agents/msr_agent_1"]

# Store reward values
reward_data = []

for agent_path in agent_paths:
    # Load correct model.
    env = gym.make('msr-ident-win-v2')
    model = DQN.load(agent_path)
    model.set_env(env)

    # Save name
    save_name = agent_path.split('/')[-1]

    # Save history
    reward = save_history(env, model, save_name=save_name, view=False)

    # Save reward data
    agent_name = agent_path.split('/')[-1]
    reward_data.append((reward, agent_name))

# Print out reward data
reward_data = np.array(reward_data)
sorted_data = reward_data[reward_data[:,0].argsort()]
print(sorted_data)
np.savetxt("reward_data.txt", sorted_data, fmt='%s')
