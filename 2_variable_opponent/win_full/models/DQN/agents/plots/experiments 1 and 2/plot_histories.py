from learning_tools import plot_action_separation, plot_fatigue
histories = ["Experiment 1.hist", "Experiment 1b.hist", "Experiment 2.hist", "Experiment 2b.hist"]
titles = ["Un-normalized Observations for 15 Runs of 25000 Steps", "Un-normalized Observations for 5 Runs of 100000 Steps", "Normalised Observations for 15 Runs of 25000 Steps", "Normalised Observations for 5 runs of 100000 Steps"]
titles = ['(a) Reward = 2.0659','(b) Reward = 7.6287','(c) Reward = 3.7160','(d) Reward = 4.9978']
plot_action_separation(save=True, histories=histories, titles=titles,)
plot_fatigue(save=True)
