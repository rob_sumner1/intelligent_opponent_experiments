import os
import gym
import pickle
import numpy as np
from stable_baselines import DQN
# from stable_baselines.deepq.policies import MlpPolicy
from learning_tools import run_model
import matplotlib.pyplot as plt

# Load trained model
eval_folder = os.path.dirname(os.path.abspath(__file__)) +  \
    '/agents/experiment_3/size_11/evaluation/'
agent_path = eval_folder + 'msr_final'

# Range of distances to test
distances = [100, 150, 200, 250, 300, 350, 400, 450, 500]

# Environment and model
env = gym.make('msr-ident-win-v3')
model = DQN.load(agent_path)
model.set_env(env)

# Run model for all distances
rewards = []
sep_vals = []
for distance in distances:
    # Get reward and save histories
    obs = env.reset(distance=distance)
    hist_name = eval_folder + "hist_" + str(distance)
    reward = run_model(env, model, obs=obs, hist_save_name=hist_name)
    rewards.append(reward)

    # Get separation
    hist_file = open(hist_name + ".hist", 'rb')
    data = pickle.load(hist_file)
    hist_file.close()
    sep = data[1]['state'][-1][0] - data[2]['state'][-1][0]
    sep_vals.append(sep)

print(rewards)
print(sep_vals)

# Save data
data_file =open(eval_folder + "results.txt",'w')
data_file.write("Sprint Distance - Rewards: \n")
for i, reward in enumerate(rewards):
    data_file.write("%s - %s\n" % (env.race_distance - distances[i], reward))
data_file.write("\nSprint Distance - Separation values: \n")
for i, sep in enumerate(sep_vals):
    data_file.write("%s - %s\n" % (env.race_distance - distances[i], sep))
data_file.close()
