from learning_tools import plot_action_separation, plot_fatigue

plot_action_separation(save=True)
plot_fatigue(save=True)
