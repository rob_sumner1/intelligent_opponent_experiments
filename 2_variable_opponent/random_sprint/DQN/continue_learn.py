import os
import gym
import gym_msr
import numpy as np
from stable_baselines import DQN, results_plotter
from stable_baselines.bench import Monitor
from stable_baselines.deepq.policies import MlpPolicy
from stable_baselines.results_plotter import load_results, ts2xy
from learning_tools import get_last_agent
import matplotlib.pyplot as plt

# Define call back function
best_mean_reward, n_steps = -np.inf, 0

def callback(_locals, _globals):
    """
    Callback called at each step (for DQN an others) or after n steps (see ACER or PPO2)
    :param _locals: (dict)
    :param _globals: (dict)
    """
    global n_steps, best_mean_reward
    # Print stats every 1000 calls
    if (n_steps + 1) % 1000 == 0:
        # Evaluate policy training performance
        x, y = ts2xy(load_results(log_dir), 'timesteps')
        if len(x) > 0:
            mean_reward = np.mean(y[-100:])
            print(x[-1], 'timesteps')
            print("Best mean reward: {:.2f} - Last mean reward per episode: {:.2f}".format(best_mean_reward, mean_reward))

            # New best model, you could save the agent here
            if mean_reward > best_mean_reward:
                best_mean_reward = mean_reward
                # Example for saving best model
                print("Saving new best model")
                _locals['self'].save(agent_folder + '/msr_callback_' + str(n_steps))

    n_steps += 1
    return True

# Folder location for learning
agent_folder = os.path.dirname(os.path.abspath(__file__)) + '/agents/experiment_1/run 1/training run 2/'
agent = agent_folder + 'msr_init'
if not os.path.exists(agent_folder) or not os.path.exists(agent + ".zip"):
    raise RuntimeError("Folder or agent does not exist.")

# Make log folder
log_dir = agent_folder + '/log'
os.makedirs(log_dir, exist_ok=True)

# Reset values
#### Change this value #####
prev_best = 1.269913429999999899e+00
best_mean_reward, n_steps = prev_best, 0
time_steps = 300000

# Create random sprint environment with a monitor
env = gym.make('msr-ident-win-v3')
env = Monitor(env, log_dir, allow_early_resets=True)

# Create and train agent.
model = DQN.load(agent)
model.set_env(env)
model.learn(total_timesteps=time_steps, callback=callback)

# Visualise training results
results_plotter.plot_results([log_dir], time_steps, results_plotter.X_TIMESTEPS, "Random Sprint")
plt.savefig(agent_folder + '/learning_plot.png')
print("Best average reward achieved: ", best_mean_reward)


