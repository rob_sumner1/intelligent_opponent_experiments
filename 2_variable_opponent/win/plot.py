"""Script to plot the responses for a given model using a given opponent power.
Vary the model in the file_name variable.
Vary the power in the rider_b_control variable.
Images saved to the img_bin folder, use these to make videos.
"""

import gym
import gym_msr
import numpy as np
import pickle
from stable_baselines.common.vec_env import DummyVecEnv
from stable_baselines import DQN
from stable_baselines.deepq.policies import MlpPolicy

# # Parameters
save_images = False

# # Vectorise the environment
env_to_wrap = gym.make('msr-ident-win-v1')

# # Load the last trained agent
file_name = "models/DQN/agents/msr_agent_dqn_3"
print('Loading model: ' + file_name)
model = DQN.load(file_name)
model.set_env(env_to_wrap)

# # Visualise response
obs = env_to_wrap.reset()
i = 0
info = {}
while not env_to_wrap.complete():
    action, _states = model.predict(obs)
    obs, rewards, dones, info = env_to_wrap.step(action)
    env_to_wrap.render()
    if save_images:
        env_to_wrap.image_render(file_name='models/DQN/img_bin/image', index=int(i))
        i += 1


env_to_wrap.save_history()
env_to_wrap.close()
