"""Script to check the generalisation of the trained model.
    Model is trained on a constant race distance against an identical opponent.
    Lets see how the result changes due to different parameters.
"""

import gym
import gym_msr
import numpy as np
import pickle
from stable_baselines.common.vec_env import DummyVecEnv
from stable_baselines import DQN
from stable_baselines.deepq.policies import MlpPolicy

# # Parameters
save_images = False

# # Setup the environment
env_to_wrap = gym.make('msr-ident-win-v1')
# env_to_wrap.rider_a_fatigue_rate = 0.04
env_to_wrap.race_distance = 500

# # Load the trained agent
file_name = "../models/DQN/agents/msr_agent_dqn_3"
print('Loading model: ' + file_name)
model = DQN.load(file_name)
model.set_env(env_to_wrap)

# # Visualise response
obs = env_to_wrap.reset()
i = 0
info = {}
while not env_to_wrap.complete():
    action, _states = model.predict(obs)
    obs, rewards, dones, info = env_to_wrap.step(action)
    env_to_wrap.render()
    if save_images:
        env_to_wrap.image_render(file_name='models/DQN/img_bin/image', index=int(i))
        i += 1

file_name = "msr_fatigue_0_" + str(int(env_to_wrap.rider_a_fatigue_rate*1000))
file_name = "msr_distance_" + str(env_to_wrap.race_distance)
env_to_wrap.save_history(name=file_name)
env_to_wrap.close()
