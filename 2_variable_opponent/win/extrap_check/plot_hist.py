from learning_tools import plot_action_separation, plot_fatigue

histories = ['msr_fatigue_0_000.hist', 'msr_fatigue_0_010.hist', 'msr_fatigue_0_020.hist', 'msr_fatigue_0_030.hist', 'msr_fatigue_0_040.hist']

histories = ['msr_fatigue_0_000.hist', 'msr_fatigue_0_020.hist', 'msr_fatigue_0_040.hist']
titles = [r"$ \alpha = 0.00 $", r"$ \alpha = 0.02 $", r"$ \alpha = 0.04 $"]
sup1 = r"Action-Separation Plots for Varying Fatigue Rate $\alpha$"
sup2 = r"Fatigue Plots for Varying Fatigue Rate $\alpha$"
plot_action_separation(histories=histories, titles=titles, suptitle=sup1)
plot_fatigue(histories=histories, titles=titles, suptitle=sup2)
