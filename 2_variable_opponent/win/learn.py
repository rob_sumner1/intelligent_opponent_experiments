import gym
import gym_msr
import numpy as np
import pickle
from stable_baselines.common.vec_env import DummyVecEnv
from stable_baselines import DQN

from stable_baselines.deepq.policies import MlpPolicy
import glob
import os

# # Parameters
save_images = False
epoch_timesteps = 10000

# # Models that have been created so far
folder = os.path.dirname(os.path.abspath(__file__)) + '/models/DQN/agents/*zip'
files =  [file.split('/')[-1] for file in glob.glob(folder)]
model_nums = sorted([int(name.split('_')[-1][:-4]) for name in files])

# # Vectorise the environment
env_to_wrap = gym.make('msr-ident-win-v1')
# env = DummyVecEnv([lambda : env_to_wrap])

# # Load the last trained agent
if (len(model_nums) > 0):
    file_name = "models/DQN/agents/msr_agent_dqn_" + str(model_nums[-1])
    print('Loading model: ' + file_name)
    model = DQN.load(file_name)
    model.set_env(env_to_wrap)
else:
    print('New model...')
    model = DQN(MlpPolicy, env_to_wrap, verbose=1)
    model_nums = [0]

# # # Learn with deepQ policy
for i in range(1):
    # model.learn(total_timesteps=epoch_timesteps)
    # model.save("models/DQN/agents/msr_agent_dqn_" + str(model_nums[-1] + i + 1) )

    # # Evaluate the current model more fully.
    # evaluate(env_to_wrap, model)

    # # Visualise response
    i = 0
    info = {}
    obs = env_to_wrap.reset()
    while not env_to_wrap.complete():
        action, _states = model.predict(obs)
        print(action)
        obs, rewards, dones, info = env_to_wrap.step(action)
        env_to_wrap.render()
        if save_images:
            env_to_wrap.image_render(file_name='models/DQN/img_bin/image', index=int(i))
            i += 1
    env_to_wrap.close()
    env_to_wrap.reset()

# plot_learn_curve(os.path.dirname(os.path.abspath(__file__)), env_to_wrap, epoch_timesteps)
