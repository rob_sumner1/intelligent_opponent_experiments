import gym
import gym_msr
import numpy as np
from gym_msr.envs.msr_policies import MsrPolicy
from learning_tools import plot_action_separation

# Setup
actions, ep_returns, rewards, obs, ep_starts = [], [], [], [], []
action_dim = 100 # Large to make roughly continuous
env = gym.make('msr-duel-fixed-v2', action_dim=action_dim)
rider_a_agent = MsrPolicy(rider='Rider A')


# # # (1) Races - Expert vs Expert
# n_races = 500
# for leading_rider in ['Rider A', 'Rider B']:
#     for race in range(n_races):
#         print('Race = ', race)
#         # Run race to finish
#         env.reset(leading_rider=leading_rider, verbose=False)
#         rider_a_agent.reset()
#         while not env.complete():
#             rider_a_eq_exer = env.get_draft_exertion(drafting_rider='Rider A')
#             action = int(rider_a_agent.predict(env.state, rider_a_eq_exer)*(action_dim-1))
#             env.step(action)

#         # History data from environent
#         rider_a_state = env.match_sprint_simulation._simulation_histories[env.rider_a]['state']
#         rider_b_state = env.match_sprint_simulation._simulation_histories[env.rider_b]['state']
#         rider_a_actions = env.match_sprint_simulation._simulation_histories[env.rider_a]['control']
#         rider_b_actions = env.match_sprint_simulation._simulation_histories[env.rider_b]['control']

#         # Store values
#         for step in range(len(rider_a_actions)):
#             # Integer actions
#             actions.append( int(rider_a_actions[step][0]*4) )
#             actions.append( int(rider_b_actions[step][0]*4) )

#             # Episode starts are bools
#             if step == 0:
#                 ep_starts.append(True)
#                 ep_starts.append(True)
#             else:
#                 ep_starts.append(False)
#                 ep_starts.append(False)

#             # Observation values
#             env.state = rider_a_state[step] + rider_b_state[step]
#             obs.append( env.normalised_state() )
#             rewards.append( env.reward_function())
#             env.state = rider_b_state[step] + rider_a_state[step]
#             obs.append( env.normalised_state() )
#             rewards.append( env.reward_function())

#         # Store episode return
#         ep_returns.append(rewards[-2])
#         ep_returns.append(rewards[-1])

# # # Load previous file and combine.
# # old_file = np.load('expert_vs_expert.npz')
# # actions = np.concatenate( (actions, old_file['actions']), axis=0)
# # ep_returns =  np.concatenate( (ep_returns, old_file['episode_returns']), axis=0)
# # rewards =  np.concatenate( (rewards, old_file['rewards']), axis=0)
# # obs =  np.concatenate( (obs, old_file['obs']), axis=0)
# # ep_starts =  np.concatenate( (ep_starts, old_file['episode_starts']), axis=0)

# # Store results as file
# print('Size of actions = ', len(actions))
# print('Size of obs = ', len(obs))
# print('Size of rewards = ', len(rewards))
# print('Size of returns = ', len(ep_returns))
# print('Size of ep_starts = ', len(ep_starts))

# # Save out expert vs expert results.
# np.savez_compressed('expert_vs_expert', actions=actions, episode_returns=ep_returns,
#             rewards=rewards, obs=obs, episode_starts=ep_starts)


# # # (2) Races - Expert vs Expert (extra drafting only)
# n_races = 500
# for race in range(n_races):
#     print('Race = ', race)
#     # Run race to finish
#     env.reset(leading_rider='Rider B', verbose=False)
#     rider_a_agent.reset()
#     while not env.complete():
#         rider_a_eq_exer = env.get_draft_exertion(drafting_rider='Rider A')
#         action = int(rider_a_agent.predict(env.state, rider_a_eq_exer)*(action_dim-1))
#         env.step(action)

#     # History data from environent
#     rider_a_state = env.match_sprint_simulation._simulation_histories[env.rider_a]['state']
#     rider_b_state = env.match_sprint_simulation._simulation_histories[env.rider_b]['state']
#     rider_a_actions = env.match_sprint_simulation._simulation_histories[env.rider_a]['control']

#     # Store values
#     for step in range(len(rider_a_actions)):
#         # Integer actions
#         actions.append( int(rider_a_actions[step][0]*4) )

#         # Episode starts are bools
#         if step == 0:
#             ep_starts.append(True)
#         else:
#             ep_starts.append(False)

#         # Observation values
#         env.state = rider_a_state[step] + rider_b_state[step]
#         obs.append( env.normalised_state() )
#         rewards.append( env.reward_function())

#     # Store episode return
#     ep_returns.append(rewards[-1])

# # # Load previous file and combine.
# # old_file = np.load('expert_vs_expert_more_draft.npz')
# # actions = np.concatenate( (actions, old_file['actions']), axis=0)
# # ep_returns =  np.concatenate( (ep_returns, old_file['episode_returns']), axis=0)
# # rewards =  np.concatenate( (rewards, old_file['rewards']), axis=0)
# # obs =  np.concatenate( (obs, old_file['obs']), axis=0)
# # ep_starts =  np.concatenate( (ep_starts, old_file['episode_starts']), axis=0)

# # Store results as file
# print('Size of actions = ', len(actions))
# print('Size of obs = ', len(obs))
# print('Size of rewards = ', len(rewards))
# print('Size of returns = ', len(ep_returns))
# print('Size of ep_starts = ', len(ep_starts))
# np.savez_compressed('expert_vs_expert_more_draft', actions=actions, episode_returns=ep_returns,
#             rewards=rewards, obs=obs, episode_starts=ep_starts)


# # # (3) Races - Expert vs zero
# n_races = 125
# for leading_rider in ['Rider A', 'Rider B']:
#     for race in range(n_races):
#         print('Race = ', race)
#         # Run race to finish
#         env.reset(leading_rider=leading_rider, verbose=False)
#         rider_a_agent.reset()
#         while not env.complete():
#             env.step(0)

#         # History data from environent
#         rider_a_state = env.match_sprint_simulation._simulation_histories[env.rider_a]['state']
#         rider_b_state = env.match_sprint_simulation._simulation_histories[env.rider_b]['state']
#         rider_a_actions = env.match_sprint_simulation._simulation_histories[env.rider_a]['control']
#         rider_b_actions = env.match_sprint_simulation._simulation_histories[env.rider_b]['control']

#         # Store values
#         for step in range(len(rider_a_actions)):
#             # Integer actions
#             actions.append( int(rider_a_actions[step][0]*4) )
#             actions.append( int(rider_b_actions[step][0]*4) )

#             # Episode starts are bools
#             if step == 0:
#                 ep_starts.append(True)
#                 ep_starts.append(True)
#             else:
#                 ep_starts.append(False)
#                 ep_starts.append(False)

#             # Observation values
#             env.state = rider_a_state[step] + rider_b_state[step]
#             obs.append( env.normalised_state() )
#             rewards.append( env.reward_function())
#             env.state = rider_b_state[step] + rider_a_state[step]
#             obs.append( env.normalised_state() )
#             rewards.append( env.reward_function())

#         # Store episode return
#         ep_returns.append(rewards[-2])
#         ep_returns.append(rewards[-1])

# # # Load previous file and combine.
# # old_file = np.load('expert_vs_zeros.npz')
# # actions = np.concatenate( (actions, old_file['actions']), axis=0)
# # ep_returns =  np.concatenate( (ep_returns, old_file['episode_returns']), axis=0)
# # rewards =  np.concatenate( (rewards, old_file['rewards']), axis=0)
# # obs =  np.concatenate( (obs, old_file['obs']), axis=0)
# # ep_starts =  np.concatenate( (ep_starts, old_file['episode_starts']), axis=0)

# # Store results as file
# print('Size of actions = ', len(actions))
# print('Size of obs = ', len(obs))
# print('Size of rewards = ', len(rewards))
# print('Size of returns = ', len(ep_returns))
# print('Size of ep_starts = ', len(ep_starts))
# np.savez_compressed('expert_vs_zeros', actions=actions, episode_returns=ep_returns,
#             rewards=rewards, obs=obs, episode_starts=ep_starts)


# # # (4) Races - Expert vs ones
# n_races = 125
# for leading_rider in ['Rider A', 'Rider B']:
#     for race in range(n_races):
#         print('Race = ', race)
#         # Run race to finish
#         env.reset(leading_rider=leading_rider, verbose=False)
#         rider_a_agent.reset()
#         while not env.complete():
#             env.step(action_dim-1)

#         # History data from environent
#         rider_a_state = env.match_sprint_simulation._simulation_histories[env.rider_a]['state']
#         rider_b_state = env.match_sprint_simulation._simulation_histories[env.rider_b]['state']
#         rider_a_actions = env.match_sprint_simulation._simulation_histories[env.rider_a]['control']
#         rider_b_actions = env.match_sprint_simulation._simulation_histories[env.rider_b]['control']

#         # Store values
#         for step in range(len(rider_a_actions)):
#             # Integer actions
#             actions.append( int(rider_a_actions[step][0]*4) )
#             actions.append( int(rider_b_actions[step][0]*4) )

#             # Episode starts are bools
#             if step == 0:
#                 ep_starts.append(True)
#                 ep_starts.append(True)
#             else:
#                 ep_starts.append(False)
#                 ep_starts.append(False)

#             # Observation values
#             env.state = rider_a_state[step] + rider_b_state[step]
#             obs.append( env.normalised_state() )
#             rewards.append( env.reward_function())
#             env.state = rider_b_state[step] + rider_a_state[step]
#             obs.append( env.normalised_state() )
#             rewards.append( env.reward_function())

#         # Store episode return
#         ep_returns.append(rewards[-2])
#         ep_returns.append(rewards[-1])

# # # Load previous file and combine.
# # old_file = np.load('expert_vs_ones.npz')
# # actions = np.concatenate( (actions, old_file['actions']), axis=0)
# # ep_returns =  np.concatenate( (ep_returns, old_file['episode_returns']), axis=0)
# # rewards =  np.concatenate( (rewards, old_file['rewards']), axis=0)
# # obs =  np.concatenate( (obs, old_file['obs']), axis=0)
# # ep_starts =  np.concatenate( (ep_starts, old_file['episode_starts']), axis=0)

# # Store results as file
# print('Size of actions = ', len(actions))
# print('Size of obs = ', len(obs))
# print('Size of rewards = ', len(rewards))
# print('Size of returns = ', len(ep_returns))
# print('Size of ep_starts = ', len(ep_starts))
# np.savez_compressed('expert_vs_ones', actions=actions, episode_returns=ep_returns,
#             rewards=rewards, obs=obs, episode_starts=ep_starts)


# # # (5) Races - Expert vs early/late sprints
# n_races = 125
# rider_a_agent.min_lead_sprint_distance = 0
# rider_a_agent.max_lead_sprint_distance = 705
# rider_a_agent.min_trail_sprint_distance = 655
# rider_a_agent.min_trail_sprint_distance = 750
# for leading_rider in ['Rider A', 'Rider B']:
#     for race in range(n_races):
#         print('Race = ', race)
#         # Run race to finish
#         env.reset(leading_rider=leading_rider, verbose=False)
#         rider_a_agent.reset()
#         while not env.complete():
#             rider_a_eq_exer = env.get_draft_exertion(drafting_rider='Rider A')
#             action = int(rider_a_agent.predict(env.state, rider_a_eq_exer)*(action_dim-1))
#             env.step(action)

#         # History data from environent
#         rider_a_state = env.match_sprint_simulation._simulation_histories[env.rider_a]['state']
#         rider_b_state = env.match_sprint_simulation._simulation_histories[env.rider_b]['state']
#         rider_a_actions = env.match_sprint_simulation._simulation_histories[env.rider_a]['control']
#         rider_b_actions = env.match_sprint_simulation._simulation_histories[env.rider_b]['control']

#         # Store values
#         for step in range(len(rider_a_actions)):
#             # Integer actions
#             actions.append( int(rider_a_actions[step][0]*4) )
#             actions.append( int(rider_b_actions[step][0]*4) )

#             # Episode starts are bools
#             if step == 0:
#                 ep_starts.append(True)
#                 ep_starts.append(True)
#             else:
#                 ep_starts.append(False)
#                 ep_starts.append(False)

#             # Observation values
#             env.state = rider_a_state[step] + rider_b_state[step]
#             obs.append( env.normalised_state() )
#             rewards.append( env.reward_function())
#             env.state = rider_b_state[step] + rider_a_state[step]
#             obs.append( env.normalised_state() )
#             rewards.append( env.reward_function())

#         # Store episode return
#         ep_returns.append(rewards[-2])
#         ep_returns.append(rewards[-1])

# # # Load previous file and combine.
# # old_file = np.load('expert_vs_subopt.npz')
# # actions = np.concatenate( (actions, old_file['actions']), axis=0)
# # ep_returns =  np.concatenate( (ep_returns, old_file['episode_returns']), axis=0)
# # rewards =  np.concatenate( (rewards, old_file['rewards']), axis=0)
# # obs =  np.concatenate( (obs, old_file['obs']), axis=0)
# # ep_starts =  np.concatenate( (ep_starts, old_file['episode_starts']), axis=0)

# # Store results as file
# print('Size of actions = ', len(actions))
# print('Size of obs = ', len(obs))
# print('Size of rewards = ', len(rewards))
# print('Size of returns = ', len(ep_returns))
# print('Size of ep_starts = ', len(ep_starts))
# np.savez_compressed('expert_vs_subopt', actions=actions, episode_returns=ep_returns,
#             rewards=rewards, obs=obs, episode_starts=ep_starts)


# # (Final) - Combine results into one file
files = ['expert_vs_expert.npz', 'expert_vs_expert_more_draft.npz',
         'expert_vs_zeros.npz', 'expert_vs_ones.npz', 'expert_vs_subopt.npz']
data = np.load(files[0])
actions = data['actions']
ep_returns = data['episode_returns']
rewards =  data['rewards']
obs =  data['obs']
ep_starts = data['episode_starts']

# Concatenate all results into one file.
for i in range(1,4):
    data = np.load(files[i])
    actions = np.concatenate( (actions, data['actions']), axis=0)
    ep_returns =  np.concatenate( (ep_returns, data['episode_returns']), axis=0)
    rewards =  np.concatenate( (rewards, data['rewards']), axis=0)
    obs =  np.concatenate( (obs, data['obs']), axis=0)
    ep_starts =  np.concatenate( (ep_starts, data['episode_starts']), axis=0)

# Edit rewards
for i in range(len(rewards)):
    if rewards[i] >= 1:
        rewards[i] = 1
    else:
        rewards[i] = -1
for i in range(len(ep_returns)):
    if ep_returns[i] >= 1:
        ep_returns[i] = 1
    else:
        ep_returns[i] = -1

# Save out
actions = np.reshape(actions, (len(actions),1))
print('Actions shape = ', actions.shape)
print('Ep returns shape = ', ep_returns.shape)
print('Rewards shape = ', rewards.shape)
print('Obs shape = ', obs.shape)
print('Ep starts = ', ep_starts.shape)
np.savez_compressed('trajectories', actions=actions, episode_returns=ep_returns,
            rewards=rewards, obs=obs, episode_starts=ep_starts)