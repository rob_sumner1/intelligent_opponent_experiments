import os
import gym
import gym_msr
import numpy as np
from gym_msr.envs.msr_policies import MsrPolicy
from learning_tools import plot_action_separation

# Setup
actions, ep_returns, rewards, obs, ep_starts = [], [], [], [], []
action_dim = 100 # Large to make roughly continuous
env = gym.make('msr-duel-fixed-v2', action_dim=action_dim)
rider_a_agent = MsrPolicy(rider='Rider A')

# # # Run races against against varying rider B policy
# n_steps, n_step_runs = 100, 2
# rider_b_sprint_distances = np.linspace(0, 750, n_steps)
# for i, distance in enumerate(rider_b_sprint_distances):
#     print('Distance = ', distance)
#     lead_races_won, lead_rewards, lead_seps, lead_overtakes = 0, 0, 0, 0
#     trail_races_won, trail_rewards, trail_seps, trail_overtakes = 0, 0, 0, 0
#     for _ in range(n_step_runs):
#         for leading_rider in ['Rider A', 'Rider B']:
#             # Run race to finish
#             env.reset(leading_rider=leading_rider, verbose=False,
#                         rider_b_sprint_distance=distance)
#             rider_a_agent.reset()
#             while not env.complete():
#                 rider_a_eq_exer = env.get_draft_exertion(drafting_rider='Rider A')
#                 action = int(rider_a_agent.predict(env.state, rider_a_eq_exer)*(action_dim-1))
#                 env.step(action)

#             # History data from environent
#             rider_a_state = env.match_sprint_simulation._simulation_histories[env.rider_a]['state']
#             rider_b_state = env.match_sprint_simulation._simulation_histories[env.rider_b]['state']
#             rider_a_actions = env.match_sprint_simulation._simulation_histories[env.rider_a]['control']
#             rider_b_actions = env.match_sprint_simulation._simulation_histories[env.rider_b]['control']

#             # Store values
#             for step in range(len(rider_a_actions)):
#                 # Integer actions
#                 actions.append( int(rider_a_actions[step][0]*4) )
#                 actions.append( int(rider_b_actions[step][0]*4) )

#                 # Episode starts are bools
#                 if step == 0:
#                     ep_starts.append(True)
#                     ep_starts.append(True)
#                 else:
#                     ep_starts.append(False)
#                     ep_starts.append(False)

#                 # Observation values
#                 env.state = rider_a_state[step] + rider_b_state[step]
#                 obs.append( env.normalised_state() )
#                 rewards.append( env.reward_function())
#                 env.state = rider_b_state[step] + rider_a_state[step]
#                 obs.append( env.normalised_state() )
#                 rewards.append( env.reward_function())

#             # Store episode return
#             ep_returns.append(rewards[-2])
#             ep_returns.append(rewards[-1])

# # Load previous file and combine.
# if os.path.isfile('policy_dataset/policy_data.npz'):
#     old_file = np.load('policy_dataset/policy_data.npz')
#     actions = np.concatenate( (actions, old_file['actions']), axis=0)
#     ep_returns =  np.concatenate( (ep_returns, old_file['episode_returns']), axis=0)
#     rewards =  np.concatenate( (rewards, old_file['rewards']), axis=0)
#     obs =  np.concatenate( (obs, old_file['obs']), axis=0)
#     ep_starts =  np.concatenate( (ep_starts, old_file['episode_starts']), axis=0)

# # Save out expert vs expert results.
# print('Size of actions = ', len(actions))
# print('Size of obs = ', len(obs))
# print('Size of rewards = ', len(rewards))
# print('Size of returns = ', len(ep_returns))
# print('Size of ep_starts = ', len(ep_starts))
# np.savez_compressed('policy_dataset/policy_data',
#             actions=actions,
#             episode_returns=ep_returns,
#             rewards=rewards,
#             obs=obs,
#             episode_starts=ep_starts)


# # # Run races against constant responses
# n_races = 30
# for race in range(n_races):
#     print('Race = ', race)
#     for leading_rider in ['Rider A', 'Rider B']:
#         for response in [0, 0.25, 0.5, 0.75, 1]:
#             # Run race to finish
#             env.reset(leading_rider=leading_rider, verbose=False)
#             rider_a_agent.reset()
#             while not env.complete():
#                 env.step(int(response * (action_dim-1)))

#             # History data from environent
#             rider_a_state = env.match_sprint_simulation._simulation_histories[env.rider_a]['state']
#             rider_b_state = env.match_sprint_simulation._simulation_histories[env.rider_b]['state']
#             rider_a_actions = env.match_sprint_simulation._simulation_histories[env.rider_a]['control']
#             rider_b_actions = env.match_sprint_simulation._simulation_histories[env.rider_b]['control']

#             # Store values
#             for step in range(len(rider_a_actions)):
#                 # Integer actions
#                 actions.append( int(rider_a_actions[step][0]*4) )
#                 actions.append( int(rider_b_actions[step][0]*4) )

#                 # Episode starts are bools
#                 if step == 0:
#                     ep_starts.append(True)
#                     ep_starts.append(True)
#                 else:
#                     ep_starts.append(False)
#                     ep_starts.append(False)

#                 # Observation values
#                 env.state = rider_a_state[step] + rider_b_state[step]
#                 obs.append( env.normalised_state() )
#                 rewards.append( env.reward_function())
#                 env.state = rider_b_state[step] + rider_a_state[step]
#                 obs.append( env.normalised_state() )
#                 rewards.append( env.reward_function())

#             # Store episode return
#             ep_returns.append(rewards[-2])
#             ep_returns.append(rewards[-1])

# # Save out expert vs sub-opt results.
# print('Size of actions = ', len(actions))
# print('Size of obs = ', len(obs))
# print('Size of rewards = ', len(rewards))
# print('Size of returns = ', len(ep_returns))
# print('Size of ep_starts = ', len(ep_starts))
# np.savez_compressed('policy_dataset/constant_data',
#             actions=actions,
#             episode_returns=ep_returns,
#             rewards=rewards,
#             obs=obs,
#             episode_starts=ep_starts)


# # (Final) - Combine results into one file
files = []
data = np.load('policy_dataset/policy_data.npz')
actions = data['actions']
ep_returns = data['episode_returns']
rewards =  data['rewards']
obs =  data['obs']
ep_starts = data['episode_starts']

# Concatenate all results into one file.
data = np.load('policy_dataset/constant_data.npz')
actions = np.concatenate( (actions, data['actions']), axis=0)
ep_returns =  np.concatenate( (ep_returns, data['episode_returns']), axis=0)
rewards =  np.concatenate( (rewards, data['rewards']), axis=0)
obs =  np.concatenate( (obs, data['obs']), axis=0)
ep_starts =  np.concatenate( (ep_starts, data['episode_starts']), axis=0)

# Save out
actions = np.reshape(actions, (len(actions),1))
print('Actions shape = ', actions.shape)
print('Ep returns shape = ', ep_returns.shape)
print('Rewards shape = ', rewards.shape)
print('Obs shape = ', obs.shape)
print('Ep starts = ', ep_starts.shape)
np.savez_compressed('policy_dataset/trajectories', actions=actions, episode_returns=ep_returns,
            rewards=rewards, obs=obs, episode_starts=ep_starts)