# Suppress warnings due to prescribed package versions
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
import tensorflow as tf
tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)
import os
os.environ["KMP_WARNINGS"] = "FALSE"

import gym
import numpy as np
import gym_msr
from stable_baselines import DQN
from stable_baselines.gail import ExpertDataset
from stable_baselines.deepq.policies import MlpPolicy
from learning_tools import get_last_agent
import sys

# Class for storing outputs.
class Logger(object):
    def __init__(self, logfile):
        self.terminal = sys.stdout
        self.log = open(logfile, "a")

    def write(self, message):
        self.terminal.write(message)
        self.log.write(message)

    def flush(self):
        #this flush method is needed for python 3 compatibility.
        #this handles the flush command by doing nothing.
        #you might want to specify some extra behavior here.
        pass

# Folders and locations
current_folder = os.path.dirname(os.path.abspath(__file__))
traj_file = current_folder + '/trajectories/trajectories_3.npz'
agent_folder = current_folder + '/training_runs/'
log_file = agent_folder + 'pretrain.log'
if os.path.exists(log_file):
    raise RuntimeError('Remember to delete/rename existing log file.')
os.makedirs(agent_folder, exist_ok=True)
last_agent, next_agent = get_last_agent(agent_folder, 'pretrain_')

# Initialise model
env = gym.make('msr-duel-fixed-v2')
model = DQN(MlpPolicy, env, gamma=0.997, buffer_size=250000, batch_size=256, exploration_fraction=0.12952,
            target_network_update_freq=500, prioritized_replay=False, verbose=1,
            tensorboard_log="./tensorboard_logging/")

# Store output for loss plots.
last_out = sys.stdout
sys.stdout = Logger(log_file)

# Pretrain the DQN model
val_interval = 20
n_epochs = 1000
dataset = ExpertDataset(expert_path=traj_file, batch_size=250)
model.pretrain(dataset, n_epochs=n_epochs, val_interval=val_interval)
model.save(next_agent)

# Delete the log file to stop
sys.stdout = last_out

# Parse log file to create a training loss plot
loss_data = []
with open(log_file) as log:
    for line in log:
        if 'Training loss:' in line:
            data = line.split(',')
            epoch = (len(loss_data) + 1) * val_interval
            train_loss = float(data[0].split(':')[1])
            val_loss = float(data[1].split(':')[1])
            loss_data.append([epoch, train_loss, val_loss])
loss_data = np.array(loss_data)
np.save(agent_folder + 'loss_data_array', loss_data)



