import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
import tensorflow as tf
tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)
import os
os.environ["KMP_WARNINGS"] = "FALSE"

import gym
from stable_baselines import DQN
from learning_tools import analyse_trained_policies, plot_policy_analysis, plot_action_separation, measure_win_rate
import matplotlib.pyplot as plt
import numpy as np

# Load agent - edit here...
agent_file = "pretrain_5"
agent_folder = os.path.dirname(os.path.abspath(__file__)) + '/training_runs/epoch_2000/'
agent_filepath = agent_folder + agent_file + '.zip'
rider_a_agent = DQN.load(agent_filepath)

# # Run analysis of policy.
save_folder = agent_folder + 'policy_analysis/'
os.makedirs(save_folder, exist_ok=True)
save_name = save_folder + 'policy_analysis_' + agent_file
results = analyse_trained_policies(rider_a_agent, n_steps=100,
                                    n_step_runs=100, save_name=save_name)

# plot_policy_analysis([save_name], labels=None, show_sprint_zones=True,
#                      show=True, legend_loc='lower right',
#                      save_name=save_folder+'policy_plot')

# Fatigue rate investigation
#rider_a_fatigue_rates = np.linspace(0.009,0.0115, 10)
#rider_b_fatigue_rates = np.linspace(0.009,0.0115, 10)
#overall = np.zeros( (len(rider_b_fatigue_rates), len(rider_a_fatigue_rates)) )
#lead =  np.zeros( (len(rider_b_fatigue_rates), len(rider_a_fatigue_rates)) )
#trail =  np.zeros( (len(rider_b_fatigue_rates), len(rider_a_fatigue_rates)) )

# Vary fatigue and measure result
#for i, rider_a_fatigue in enumerate(rider_a_fatigue_rates):
#    for j, rider_b_fatigue in enumerate(rider_b_fatigue_rates):
#        env = gym.make('msr-duel-fixed-v2',
#            rider_a_fatigue_rate=rider_a_fatigue,
#            rider_b_fatigue_rate=rider_b_fatigue)
#        overall_wr, lead_wr, trail_wr = measure_win_rate(env, rider_a_agent, n_runs=75)
#        overall[i,j] = overall_wr
#        lead[i,j] = lead_wr
#        trail[i,j] = trail_wr

# Save out raw arrays.
#np.save(save_folder + 'overall_win_rate.npy', overall)
#np.save(save_folder + 'trail_win_rate', trail)
#np.save(save_folder + 'lead_win_rate', lead)

#print('Overall: ', overall)
#print('Trail: ', trail)
#print('Lead: ', lead)

