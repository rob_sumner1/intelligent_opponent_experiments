import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
import tensorflow as tf
tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)
import os
os.environ["KMP_WARNINGS"] = "FALSE"

import gym
from learning_tools import plot_action_separation, plot_fatigue, plot_velocities
from stable_baselines import DQN

# Load trained model
eval_folder = os.path.dirname(os.path.abspath(__file__)) + "/training_runs/traj_3_epoch_5000/"
agent_path = eval_folder + 'pretrain_1'

# # Environment and model
env = gym.make('msr-duel-fixed-v2')
model = DQN.load(agent_path)
model.set_env(env)

# # Run some races against policy.
n_races = 3
for i in range(n_races):
    state = env.reset(leading_rider='Rider A', verbose=True)
    info = {}
    while not env.complete():
        action, _ = model.predict(state, deterministic=True)
        state, _, _, info = env.step(action)

    env.save_history(name='msr_policy')
    plot_action_separation(histories=['msr_policy.hist'],
                        suptitle='MSR Policy: Action-Separation',
                        titles=['Rider A Leads'],
                        single_althete=False,
                        save=True,
                        image_save_name= eval_folder + '/act_sep_lead' + str(i),
                        show=False)

for i in range(n_races):
    state = env.reset(leading_rider='Rider B', verbose=True)
    info = {}
    while not env.complete():
        action, _ = model.predict(state, deterministic=True)
        state, _, _, info = env.step(action)

    env.save_history(name='msr_policy')
    plot_action_separation(histories=['msr_policy.hist'],
                        suptitle='MSR Policy: Action-Separation',
                        titles=['Rider A Trails'],
                        single_althete=False,
                        save=True,
                        image_save_name= eval_folder + '/act_sep_trail' + str(i),
                        show=False)
del env, model


# # Test against bad policies
env = gym.make('msr-duel-fixed-v3')
results = [['None', 'None'], ['None', 'None']]

# Rider B is being controlled by trained agent in this setup.
for i, leading_rider in enumerate(['Rider A', 'Rider B']):
    # All zeros
    env.reset(leading_rider=leading_rider, policy_file_path=agent_path,
                verbose=False)
    while not env.complete():
        env.step(0)
    if env.reward >= 1:
        results[0][1-i] = 'Loss'
    elif env.reward < 0:
        results[0][1-i] = 'Timed Out'
    else:
        results[0][1-i] = 'Win'

    # All ones
    max_action = env.action_space.n - 1
    env.reset(leading_rider=leading_rider, policy_file_path=agent_path,
                verbose=False)
    while not env.complete():
        env.step(max_action)
    if env.reward >= 1:
        results[1][1-i] = 'Loss'
    elif env.reward < 0:
        results[1][1-i] = 'Timed Out'
    else:
        results[1][1-i] = 'Win'
print(results)