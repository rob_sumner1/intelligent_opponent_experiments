import os
import glob
import numpy as np
import matplotlib.pyplot as plt

"""Plot the loss results from a set of training runs. This requires the .npy loss files produced
    from the pretraining script."""

# training_folders = ['traj_3_epoch_2000', 'traj_3_epoch_3000',
#             'traj_3b_epoch_2000', 'traj_3b_epoch_3000']
training_folders = ['traj_3_epoch_5000']

from matplotlib import rc
rc('font', **{'family':'CMU Serif'})
rc('text', usetex=True)

for folder in training_folders:
    # Check which .npy files are present in the folder
    all_files =  glob.glob(folder + "/*.npy")

    # Load loss files as numpy arrays
    arrays = []
    for file in all_files:
        arrays.append(np.load(file))

    # Plot all training loss curves
    max_loss = 0
    min_loss = 1e10
    for i, array in enumerate(arrays):
        max_loss = np.max([max_loss, np.max(array[:, 1:3])])
        min_loss = np.min([min_loss, np.min(array[:, 1:3])])
        plt.plot(array[:,0], array[:,1], label='Run '+ str(i))
    plt.ylim([min_loss - 0.1*max_loss, 1.1*max_loss])
    plt.title('Training Loss for all Learning Runs')
    plt.xlabel('Epochs')
    plt.ylabel('Loss')
    plt.legend()
    plt.tight_layout()
    plt.savefig(folder + '/all_runs', dpi=480)
    plt.close()

    # Plot train/validate plots
    for i, array in enumerate(arrays):
        plt.plot(array[:,0], array[:,1], color='tab:red', label='Training')
        plt.plot(array[:,0], array[:,2], color='tab:green', label='Validation')
        plt.ylim([min_loss - 0.1*max_loss, 1.1*max_loss])
        plt.title('Training and Validation Loss for Run ' + str(i))
        plt.xlabel('Epochs')
        plt.ylabel('Loss')
        plt.legend()
        plt.tight_layout()
        plt.savefig(folder + '/run_' + str(i), dpi=480)
        plt.close()