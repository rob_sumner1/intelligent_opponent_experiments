import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
import tensorflow as tf
tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)
import os
os.environ["KMP_WARNINGS"] = "FALSE"

import gym
from stable_baselines import DQN
from gym_msr.envs.msr_policies import MsrPolicy
from learning_tools import analyse_policies, plot_policy_analysis, plot_action_separation, measure_win_rate
import matplotlib.pyplot as plt
import numpy as np

# Agent a file save locations
current_folder = os.path.dirname(os.path.abspath(__file__))
agent_folder = current_folder + '/traj_3_epoch_5000/'
agent_file = "pretrain_1"
os.makedirs(agent_folder, exist_ok=True)
save_name = agent_folder + 'full_policy_analysis_' + agent_file

# Expert policy files for comparison
expert_folder = '/'.join(current_folder.split('/')[:-2]) + '/6_policy_analysis/expert/'
expert_5 = expert_folder + 'full_policy_analysis_size_5.npy'
expert_200 = expert_folder + 'full_policy_analysis_size_200.npy'

# Set/Load agent
agent_filepath = agent_folder + agent_file + '.zip'
rider_a_agent = DQN.load(agent_filepath)

# Run analysis of policy
results = analyse_policies(rider_a_agent, n_steps=20, n_step_runs=10,
				            save_name=save_name, action_dim=5)

# Plot policy analysis
plot_policy_analysis([expert_5, save_name],
                      labels=['Expert Policy $|A|=5$', agent_file.replace('_', ' ')],
                      show=True, legend_loc='lower right',
                      save_name=agent_folder+'policy_plot_5_'+ agent_file)
plot_policy_analysis([expert_200, save_name],
                      labels=['Expert Policy $|A|=200$', agent_file.replace('_', ' ')],
                      show=True, legend_loc='lower right',
                      save_name=agent_folder+'policy_plot_200_'+ agent_file)
