import gym
import gym_msr
import numpy as np
import pickle
from stable_baselines.common.vec_env import DummyVecEnv
from stable_baselines import DQN

from stable_baselines.deepq.policies import MlpPolicy
import glob
import os
from learning_tools import evaluate

# # Parameters
save_images = False
epoch_timesteps = 100000

# # Models that have been created so far
folder = os.path.dirname(os.path.abspath(__file__)) + '/models/DQN/agents/*zip'
files =  [file.split('/')[-1] for file in glob.glob(folder)]
model_nums = sorted([int(name.split('_')[-1][:-4]) for name in files])

# # Vectorise the environment
env_to_wrap = gym.make('msr-fixed-win-v0')
env = DummyVecEnv([lambda : env_to_wrap])

# # Load the last trained agent
if (len(model_nums) > 0):
    model = DQN.load("models/DQN/agents/msr_agent_dqn_" + str(model_nums[-1]) )
    model.set_env(env)
else:
    model = DQN(MlpPolicy, env_to_wrap, verbose=1)
    model_nums = [0]

# # Learn with deepQ policy
model.learn(total_timesteps=epoch_timesteps)
model.save("models/DQN/agents/msr_agent_dqn_" + str(model_nums[-1] + 1) )

# # Evaluate the current model more fully.
evaluate(env_to_wrap, model)

# # Visualise response
obs = env_to_wrap.reset()
env_to_wrap.rider_b_control = [100]
i = 0
info = {}
while not env_to_wrap.complete():
    action, _states = model.predict(obs)
    obs, rewards, dones, info = env_to_wrap.step(action)
    env_to_wrap.render()
    if save_images:
        env_to_wrap.image_render(file_name='models/DQN/image', index=int(i))
        i += 1

env.close()
