import numpy as np
import pickle
import matplotlib.pyplot as plt
import lapysim


file = open('msr_50.hist', 'rb')
vals_50 = pickle.load(open('msr_50.hist', 'rb'))
vals_100 = pickle.load(open('msr_100.hist', 'rb'))
vals_150 = pickle.load(open('msr_150.hist', 'rb'))
vals_200 = pickle.load(open('msr_200.hist', 'rb'))

# # Set latex interpreter
from matplotlib import rc
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
rc('text', usetex=True)

# # Plot for 50 power value.
ax1 = plt.subplot(221)
time = vals_50[0]
rider_a = vals_50[1]
rider_b = vals_50[2]
state_a = np.reshape(np.array(rider_a['state']), (-1,2))
state_b = np.reshape(np.array(rider_b['state']), (-1,2))

color = 'tab:red'
# ax1.set_xlabel('Time (s)')
ax1.set_ylabel('Control', color=color)
ax1.plot(time, rider_a['control'], color=color)
ax2 = ax1.twinx()
color = 'tab:blue'
# ax2.set_ylabel('Separation (m)', color=color)
ax2.set_ylim([-10,20])
plt.plot(time, state_b[:,0] - state_a[:,0], color=color)
plt.title('Power = 50')


# # Plot for 100 power value.
ax1 = plt.subplot(222)
time = vals_100[0]
rider_a = vals_100[1]
rider_b = vals_100[2]
state_a = np.reshape(np.array(rider_a['state']), (-1,2))
state_b = np.reshape(np.array(rider_b['state']), (-1,2))

color = 'tab:red'
# ax1.set_xlabel('Time (s)')
# ax1.set_ylabel('Control', color=color)
ax1.plot(time, rider_a['control'], color=color)
ax2 = ax1.twinx()
color = 'tab:blue'
ax2.set_ylabel('Separation (m)', color=color)
ax2.set_ylim([-10,20])
plt.plot(time, state_b[:,0] - state_a[:,0], color=color)
plt.title('Power = 100')

# # Plot for 150 power value.
ax1 = plt.subplot(223)
time = vals_150[0]
rider_a = vals_150[1]
rider_b = vals_150[2]
state_a = np.reshape(np.array(rider_a['state']), (-1,2))
state_b = np.reshape(np.array(rider_b['state']), (-1,2))

color = 'tab:red'
ax1.set_xlabel('Time (s)')
ax1.set_ylabel('Control', color=color)
ax1.plot(time, rider_a['control'], color=color)
ax2 = ax1.twinx()
color = 'tab:blue'
# ax2.set_ylabel('Separation (m)', color=color)
ax2.set_ylim([-10,20])
plt.plot(time, state_b[:,0] - state_a[:,0], color=color)
plt.title('Power = 150')

# # Plot for 200 power value.
ax1 = plt.subplot(224)
time = vals_200[0]
rider_a = vals_200[1]
rider_b = vals_200[2]
state_a = np.reshape(np.array(rider_a['state']), (-1,2))
state_b = np.reshape(np.array(rider_b['state']), (-1,2))

color = 'tab:red'
ax1.set_xlabel('Time (s)')
# ax1.set_ylabel('Control', color=color)
ax1.plot(time, rider_a['control'], color=color)
ax2 = ax1.twinx()
color = 'tab:blue'
ax2.set_ylabel('Separation (m)', color=color)
ax2.set_ylim([-10,20])
plt.plot(time, state_b[:,0] - state_a[:,0], color=color)
plt.title('Power = 200')

# Overall title
# plt.suptitle("Agent Response for Varying Opponent Power", fontsize=14)
plt.tight_layout()
plt.savefig("plot.png", dpi=480)
plt.show()
