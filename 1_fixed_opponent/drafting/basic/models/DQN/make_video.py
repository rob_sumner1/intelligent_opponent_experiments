import gym
import gym_msr
from stable_baselines.common.vec_env import DummyVecEnv
from stable_baselines import DQN, PPO2

from stable_baselines.deepq.policies import MlpPolicy

# # Vectorise the environment
env_to_wrap = gym.make('msr-fixed-draft-v0')
env = DummyVecEnv([lambda : env_to_wrap])

# # Load the trained agent
model = DQN.load("agents/msr_agent_dqn_7")
model.set_env(env)

# # Visualise response
obs = env_to_wrap.reset()
print(env_to_wrap.rider_b_control)
i = 0
info = {}
while not env_to_wrap.complete():
    action, _states = model.predict(obs)
    obs, rewards, dones, info = env_to_wrap.step(action)
    env_to_wrap.render()
    env_to_wrap.image_render(file_name='img_bin/image', index=int(i))
    i += 1

print(info['total_reward'])
env.close()
