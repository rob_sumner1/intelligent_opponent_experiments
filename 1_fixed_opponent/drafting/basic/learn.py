import gym
import gym_msr
import numpy as np
import pickle
from stable_baselines.common.vec_env import DummyVecEnv
from stable_baselines import DQN, PPO2

from stable_baselines.deepq.policies import MlpPolicy
# from stable_baselines.common.policies import MlpPolicy

# # Vectorise the environment
env_to_wrap = gym.make('msr-fixed-draft-v1')
env = DummyVecEnv([lambda : env_to_wrap])

# # Load the trained agent
# model = PPO2.load("msr_agent_ppo2")
model = DQN.load("models/DQN/agents/msr_agent_dqn_7")
model.set_env(env)

# # Learn with deepQ policy
# model = DQN(MlpPolicy, env_to_wrap, verbose=1)
# model.learn(total_timesteps=25000)
# model.save("models/DQN/msr_agent_dqn")

# # Learn with proximal policy.
# model = PPO2(MlpPolicy, env, verbose=1)
# model.learn(total_timesteps=150000)
# model.save("msr_agent_ppo2")

# # Visualise response
obs = env_to_wrap.reset()
env_to_wrap.rider_b_control = [200]
i = 0
info = {}
while not env_to_wrap.complete():
    action, _states = model.predict(obs)
    obs, rewards, dones, info = env_to_wrap.step(action)
    env_to_wrap.render()
    # env_to_wrap.image_render(file_name='models/DQN/image', index=int(i))
    # i += 1

print("Total reward = " + str(info['total_reward']))
env.close()


# # Save histories
# def save_responses(power):
#     obs = env_to_wrap.reset()
#     env_to_wrap.rider_b_control = [power]
#     while not env_to_wrap.complete():
#         action, _states = model.predict(obs)
#         obs, rewards, dones, info = env_to_wrap.step(action)

#     env_to_wrap.save_history("models/DQN", "msr_" + str(power))
#     return

# save_responses(50)
# save_responses(100)
# save_responses(150)
# save_responses(200)



