from learning_tools import plot_rider_histories

plot_rider_histories(histories=["p_50.hist", "p_100.hist", "p_150.hist"],save=True)
