#!/bin/bash
echo "Beginning batch learn..." > learn_log.txt
for i in 1 2 3 4 5
do
   echo "Learning Run: {$i}" >> learn_log.txt
   python  learn.py 2>&1 | tee -a learn_log.txt
done
